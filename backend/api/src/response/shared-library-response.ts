import { BaseResponse } from "./base-response";
import { SharedLibrary } from "../models";

/**
 * Shared Library Respponse Class
 */


export class SharedLibraryResponse {
    sharedLibrary: SharedLibrary;
    response: BaseResponse;
}


