import { BaseResponse } from "./base-response";
import { Library } from "../models";

/**
 * Library Respponse Class
 */

export class LibraryResponse {
  library: Library;
  response: BaseResponse;


}
