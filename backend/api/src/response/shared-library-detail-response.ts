import { model, property, Model } from '@loopback/repository';
import { SharedLibrary } from '../models';

/**
 * Model for Shared Library Detail Respponse 
 */
@model()
export class SharedLibraryDetail {
  @property({
    type: 'number',
    id: true,
  })
  id: number;

  libraryId: number;

  userId: number;

  libraryName: string;

  @property({
    type: 'number',
  })
  sharedUserId: number;

  @property({
    type: 'string',
  })
  sharedUserName: string;

  @property({
    type: 'date',
  })
  dateCreated?: string;

  constructor(data: SharedLibrary) {
    this.id = data.id;
    this.userId = data.userId;
    this.sharedUserId = data.sharedUserId;
    this.libraryId = data.libraryId;
    this.dateCreated = data.dateCreated;
  }
}
