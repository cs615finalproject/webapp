import { property } from "@loopback/repository";

/**
 * User List Entity Class
 */

export class UserListEntity {

  @property({
    type: 'number',
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
  })
  imageUrl?: string;

}
