import { BaseResponse } from "./base-response";
import { Library } from "../models";

/**
 * Library List Respponse Class
 */

export class LibraryListResponse {
  library: Library[];
  response: BaseResponse;


}
