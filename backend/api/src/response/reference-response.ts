import { Reference } from "../models";
import { BaseResponse } from "./base-response";

/**
 * Reference Respponse Class
 */

export class ReferenceResponse {
    reference: Reference;
    response: BaseResponse;
  
  }