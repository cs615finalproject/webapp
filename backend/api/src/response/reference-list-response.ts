import { Reference } from "../models";
import { BaseResponse } from "./base-response";

/**
 * Reference List Respponse Class
 */

export class ReferenceListResponse {
  reference: Reference[];
  libraryName: string;
  libraryID: number;
  response: BaseResponse;
  res: Promise<Reference[]>;

}
