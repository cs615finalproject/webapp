import { BaseResponse } from "./base-response";
import { User } from "../models";

/**
 * User Respponse Class
 */

export class UserResponse {
  user: User;
  token: string;
  response: BaseResponse;


}
