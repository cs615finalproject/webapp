import { BaseResponse } from "./base-response";
import { SharedLibrary } from "../models";

/**
 * Shared Library List Respponse Class
 */

export class SharedLibraryListResponse {
    sharedLibrary: SharedLibrary[];
    response: BaseResponse;
}