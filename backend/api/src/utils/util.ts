import { AppConstant } from "../constant/app-constant";

export function randomToken(): string {
  return Math.round(Math.random() * (99999 - 10000) + 10000) + "";
}

export function sendEmail(to: string, content: string, subject: string) {
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(AppConstant.SENDGRID_KEY);
  const msg = {
    to: to,
    from: 'bibliographymanager@gmail.com',
    subject: subject,
    text: content
  };
  sgMail.send(msg);
}
