import { model, property, Entity } from "@loopback/repository";
import { User } from "../models";

/**
 * Model for Sharing library Request
 */

@model()
export class ShareLibraryReq extends Entity {

@property({
    type: 'number',
    required: true,
    })
    libraryId: number;

  @property({
    type: 'number',
    required: true,
  })
  sharedUserId: number;

  constructor(data?: Partial<ShareLibraryReq>) {
      super(data);
  }
}
