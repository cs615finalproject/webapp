import { property, Model, model } from "@loopback/repository";
/**
 * Model for Chage Password Request
 */
@model()
export class ChangePasswordReq {
  @property({
    type: 'string',
    required: true,
  })
  currentPassword: string;

  @property({
    type: 'string',
    required: true,
  })
  newPassword: string;
}
