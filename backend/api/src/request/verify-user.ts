import { property, Model, model } from "@loopback/repository";

/**
 * Model for Verifying User Request
 */

@model()
export class VerifyUserReq {
  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  token: string;
}
