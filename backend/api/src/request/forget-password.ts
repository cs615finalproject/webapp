import { property, Model, model } from "@loopback/repository";

/**
 * Model for forget Password Request
 */

@model()
export class ForgetPasswordReq {
  @property({
    type: 'string',
    required: true,
  })
  email: string;
}
