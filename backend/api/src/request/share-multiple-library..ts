import { model, property, Entity } from "@loopback/repository";
import { User } from "../models";

/**
 * Model for sharing multiple libraries Request
 */

@model()
export class ShareMultipleLibraryReq {

  @property({
    type: 'string',
    required: true,
  })
  libraryIds: string;

  @property({
    type: 'string',
    required: true,
  })
  sharedUserIds: string;
}
