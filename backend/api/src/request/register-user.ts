import { Model, model, property } from "@loopback/repository";

/**
 * Model for Registering User Request
 */

@model()
export class RegisterUserReq {

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;
}
