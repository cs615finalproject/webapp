import { Model, model, property } from "@loopback/repository";

/**
 * Model for Updating user Request
 */

@model()
export class UpdateUserReq {

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  imageUrl: string;

}
