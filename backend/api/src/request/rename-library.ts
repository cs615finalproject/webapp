import { Entity, model, property } from '@loopback/repository';

/**
 * Model for renaming Library Request
 */

@model()
export class RenameLibrary extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  constructor(data?: Partial<RenameLibrary>) {
    super(data);
  }
}
