import { property, Model, model } from "@loopback/repository";

/**
 * Model for Login Request
 */

@model()
export class LoginUserReq {
  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;
}
