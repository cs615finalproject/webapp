import { Model, model, property } from "@loopback/repository";

/**
 * Model for Create Library Request
 */

@model()
export class CreateLibraryReq {

  @property({
    type: 'string',
  })
  name: string;
}
