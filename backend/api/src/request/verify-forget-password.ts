import { property, Model, model } from "@loopback/repository";

/**
 * Model for verifying forget password Request
 */

@model()
export class VerifyForgetPasswordReq {
  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  token: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

}
