import { Model, model, property } from "@loopback/repository";

/**
 * Model for Create Reference Request
 */

@model()
export class CreateReferenceReq {

    @property({
        type: 'number',
      })
      entryType?: number;
    
      @property({
        type: 'string',
      })
      author?: string;
    
      @property({
        type: 'string',
      })
      bookTitle?: string;
    
      @property({
        type: 'string',
      })
      editor?: string;
    
      @property({
        type: 'string',
      })
      title?: string;
    
      @property({
        type: 'string',
      })
      journal?: string;
    
      @property({
        type: 'string',
      })
      publisher?: string;
    
      @property({
        type: 'string',
      })
      year?: string;
    
      @property({
        type: 'string',
      })
      volume?: string;
    
}
