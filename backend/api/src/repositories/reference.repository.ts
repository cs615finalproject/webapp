import { DefaultCrudRepository, BelongsToAccessor, repository, HasManyRepositoryFactory } from '@loopback/repository';
import { Reference, Library } from '../models';
import { MysqlDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { LibraryRepository } from '.';

/**
 * Repository for References
 */

export class ReferenceRepository extends DefaultCrudRepository<
  Reference,
  typeof Reference.prototype.id
  > {
  public readonly libraryId: BelongsToAccessor<
    Library,
    typeof Reference.prototype.id
  >;
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(Reference, dataSource);
  }
}
