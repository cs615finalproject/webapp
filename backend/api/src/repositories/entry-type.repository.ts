import {DefaultCrudRepository} from '@loopback/repository';
import {EntryType} from '../models';
import {MysqlDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EntryTypeRepository extends DefaultCrudRepository<
  EntryType,
  typeof EntryType.prototype.Id
> {
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
  ) {
    super(EntryType, dataSource);
  }
}
