import { DefaultCrudRepository, HasManyRepositoryFactory, repository } from '@loopback/repository';
import { User, Library, SharedLibrary } from '../models';
import { MysqlDataSource } from '../datasources';
import { inject } from '@loopback/core';
import { LibraryRepository } from './library.repository';
import { SharedLibraryRepository } from './shared-library.repository';
export type Credentials = {
  email: string;
  password: string;
};

/**
 * Repository for Users
 */

export class UserRepository extends DefaultCrudRepository<
  User,
  typeof User.prototype.id
  > {
  public libraries: HasManyRepositoryFactory<Library, typeof User.prototype.id>;
  public sharedlibraries: HasManyRepositoryFactory<SharedLibrary, typeof User.prototype.id>;
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
    @repository(LibraryRepository) protected libraryRepository: LibraryRepository,
    @repository(SharedLibraryRepository) protected sharedLibraryRepository: SharedLibraryRepository,
  ) {
    super(User, dataSource);
    this.libraries = this.createHasManyRepositoryFactoryFor(
      'libraries',
      async () => libraryRepository,
    );
    this.sharedlibraries = this.createHasManyRepositoryFactoryFor(
      'sharedlibraries',
      async () => sharedLibraryRepository,
    );
  }
}
