import { DefaultCrudRepository, BelongsToAccessor } from '@loopback/repository';
import { SharedLibrary, User, Library } from '../models';
import { MysqlDataSource } from '../datasources';
import { inject } from '@loopback/core';

/**
 * Repository for Shared Libraries
 */

export class SharedLibraryRepository extends DefaultCrudRepository<
  SharedLibrary,
  typeof SharedLibrary.prototype.id
  > {
  public readonly userId: BelongsToAccessor<
    User,
    typeof SharedLibrary.prototype.id
  >;
  public readonly libraryId: BelongsToAccessor<
    Library,
    typeof SharedLibrary.prototype.id
  >;
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource
  ) {
    super(SharedLibrary, dataSource);
  }
}
