import { DefaultCrudRepository, BelongsToAccessor, repository, HasManyRepositoryFactory } from '@loopback/repository';
import { Library, User, Reference } from '../models';
import { MysqlDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { UserRepository } from './user.repository';
import { ReferenceRepository } from './reference.repository';

/**
 * Repository for library
 */

export class LibraryRepository extends DefaultCrudRepository<
  Library,
  typeof Library.prototype.id
  > {
  public readonly userId: BelongsToAccessor<
    User,
    typeof Library.prototype.id
  >;
  public references: HasManyRepositoryFactory<Reference, typeof Library.prototype.id>;
  constructor(
    @inject('datasources.mysql') dataSource: MysqlDataSource,
    @repository(ReferenceRepository) protected referenceRepository: ReferenceRepository,
  ) {
    super(Library, dataSource);
    this.references = this.createHasManyRepositoryFactoryFor(
      'references',
      async () => referenceRepository,
    );
  }
}
