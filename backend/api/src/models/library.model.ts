import { Entity, model, property, belongsTo, hasMany } from '@loopback/repository';
import { User } from './user.model';
import { Reference } from './reference.model';

@model()
export class Library extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @belongsTo(() => User)
  userId: number;

  @property({
    type: 'string',
  })
  type: string;

  @property({
    type: 'boolean',
    default: 0,
  })
  isDeleted: boolean;

  @property({
    type: 'boolean',
    default: 1,
  })
  canBeDeleted: boolean;

  @property({
    type: 'boolean',
    default: 0,
  })
  isActive: boolean;

  @property({
    type: 'date',
    defaultFn: "now"
  })
  dateCreated: string;

  @hasMany(() => Reference)
  references: Reference[];


  constructor(data?: Partial<Library>) {
    super(data);
  }
}
