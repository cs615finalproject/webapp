import { Entity, model, property } from '@loopback/repository';

@model()
export class EntryType extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  Id: number;

  @property({
    type: 'string',
  })
  name?: string;


  constructor(data?: Partial<EntryType>) {
    super(data);
  }
}
