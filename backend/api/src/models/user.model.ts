import { Entity, model, property, hasMany } from '@loopback/repository';
import { Library } from './library.model';
import { SharedLibrary } from './shared-library.model';

@model()
export class User extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'boolean',
    default: 0,
  })
  emailVerified?: boolean;

  @property({
    type: 'string',
  })
  tokenActivate?: string;

  @property({
    type: 'string',
  })
  tokenForget?: string;

  @property({
    type: 'string',
  })
  imageUrl?: string;

  @property({
    type: 'boolean',
    default: 1,
  })
  status?: boolean;

  @property({
    type: 'date',
    defaultFn: 'now'
  })
  dateCreated?: string;

  @hasMany(() => Library)
  libraries: Library[];

  @hasMany(() => SharedLibrary)
  sharedlibraries: SharedLibrary[];


  constructor(data?: Partial<User>) {
    super(data);
  }

}
