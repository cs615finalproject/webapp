import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Library } from '.';

@model()
export class Reference extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @property({
    type: 'number',
  })
  entryType?: number;

  @property({
    type: 'string',
  })
  author?: string;

  @property({
    type: 'string',
  })
  bookTitle?: string;

  @property({
    type: 'string',
  })
  editor?: string;

  @property({
    type: 'string',
  })
  title?: string;

  @property({
    type: 'string',
  })
  journal?: string;

  @property({
    type: 'string',
  })
  publisher?: string;

  @property({
    type: 'string',
  })
  year?: string;

  @property({
    type: 'string',
  })
  volume?: string;

  @belongsTo(() => Library)
  libraryId: number;

  @property({
    type: 'boolean',
  })
  isDeleted?: boolean;

  @property({
    type: 'date',
    defaultFn: 'now'
  })
  dateCreated?: string;

  constructor(data?: Partial<Reference>) {
    super(data);
  }
}
