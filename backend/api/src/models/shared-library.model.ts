import { Entity, model, property, belongsTo } from '@loopback/repository';
import { Library } from './library.model';
import { User } from '.';

@model()
export class SharedLibrary extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id: number;

  @belongsTo(() => Library)
  libraryId: number;

  @belongsTo(() => User, { keyTo: 'id' })
  userId: number;

  @property({
    type: 'number',
    required: true,
  })
  sharedUserId: number;

  @property({
    type: 'date',
    defaultFn: 'now'
  })
  dateCreated?: string;

  constructor(data?: Partial<SharedLibrary>) {
    super(data);
  }
}
