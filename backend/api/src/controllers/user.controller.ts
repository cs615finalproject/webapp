import {
  repository,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  patch,
  requestBody,
} from '@loopback/rest';
import { User, Library } from '../models';
import { UserRepository, LibraryRepository } from '../repositories';
import { PasswordHasher } from '../services/hash.password.bcryptjs';
import { PasswordHasherBindings, JWTAuthenticationBindings } from '../keys';
import { inject, Setter } from '@loopback/core';
import { randomToken, sendEmail } from '../utils/util';
import { RegisterUserReq } from '../request/register-user';
import { BaseResponse } from '../response/base-response';
import { LoginUserReq } from '../request/login-user';
import { UserResponse } from '../response/user-response';
import { VerifyUserReq } from '../request/verify-user';
import { ChangePasswordReq } from '../request/change-password';
import { UpdateUserReq } from '../request/update-user';
import { ForgetPasswordReq } from '../request/forget-password';
import { VerifyForgetPasswordReq } from '../request/verify-forget-password';
import { authenticate, AuthenticationBindings, UserProfile } from '@loopback/authentication';
import { JWTAuthenticationService } from '../services/JWT.authentication.service';
import { UserListEntity } from '../response/user-list-reponse';

const UserProfileSchema = {
  type: 'object',
  required: ['id'],
  properties: {
    id: { type: 'string' },
    email: { type: 'string' },
    name: { type: 'string' },
  },
};

/**
 * User Controller
 */

export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(LibraryRepository)
    public libraryRepository: LibraryRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHahser: PasswordHasher,
    @inject.setter(AuthenticationBindings.CURRENT_USER)
    public setCurrentUser: Setter<UserProfile>,
    @inject(JWTAuthenticationBindings.SERVICE)
    public jwtAuthenticationService: JWTAuthenticationService,
  ) { }
/**
 * Registering a user and creating Trash and Unfil for the user
 */

  @post('/users/register', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: { 'x-ts-type': BaseResponse } } },
      },
    },
  })
  async create(@requestBody() user: RegisterUserReq): Promise<BaseResponse> {

    var response: BaseResponse;

    const foundUser = await this.userRepository.findOne({
      where: { email: user.email },
    });
    if (foundUser) {
      response = new BaseResponse(-1, "User already registered");
    } else {

      var regUser: User = new User(user);
      regUser.password = await this.passwordHahser.hashPassword(user.password);
      regUser.tokenActivate = randomToken();
      regUser.emailVerified = false;
      regUser.status = true;

      regUser = await this.userRepository.create(regUser);

      var library: Library = new Library();
      library.canBeDeleted = false;
      library.userId = regUser.id;
      library.name = "Trash";
      library.type = "Trash";

      await this.libraryRepository.create(library);

      library.name = "Unfiled";
      library.type = "Unfiled";
      await this.libraryRepository.create(library);


      var content = `Thanks for registering. your code is ${regUser.tokenActivate}`;
      sendEmail(regUser.email, content, 'Verify your account');

      response = new BaseResponse(1, "User registered, check your email to verify your account");

    }
    return response;
  }

  /**
   * Getting Current User
   */

  @get('/users/me', {
    responses: {
      '200': {
        description: 'The current user profile',
        content: {
          'application/json': {
            schema: UserProfileSchema,
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async printCurrentUser(
    @inject('authentication.currentUser') currentUser: UserProfile,
  ): Promise<UserProfile> {
    return currentUser;
  }

  /**
   * Login
   */

  @post('/users/login', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: { 'x-ts-type': UserResponse } } },
      },
    },
  })
  async login(
    @requestBody() credentials: LoginUserReq): Promise<UserResponse> {

    var response: UserResponse = new UserResponse();

    const foundUser = await this.userRepository.findOne({
      where: { email: credentials.email },
    });

    if (!foundUser) {
      response.response = new BaseResponse(-1, `User with email ${credentials.email} not found.`);
      return response;
    }

    const passwordMatched = await this.passwordHahser.comparePassword(
      credentials.password,
      foundUser.password,
    );

    if (!passwordMatched) {
      response.response = new BaseResponse(-2, 'The credentials are not correct');
      return response;
    }

    if (!foundUser.emailVerified) {
      response.response = new BaseResponse(-3, 'User account not verified');
      return response;
    }

    response.token = await this.jwtAuthenticationService.getAccessTokenForUserW(foundUser);
    response.user = foundUser;
    response.response = new BaseResponse(1, "Login Successful");

    return response;
  }

  /**
   * Verifying User after registration
   */

  @post('/users/verify', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: { 'x-ts-type': UserResponse } } },
      },
    },
  })
  async verify(
    @requestBody() credentials: VerifyUserReq): Promise<UserResponse> {

    var response: UserResponse = new UserResponse();

    const foundUser = await this.userRepository.findOne({
      where: { email: credentials.email },
    });

    if (!foundUser) {
      response.response = new BaseResponse(-2, `User with email ${credentials.email} not found.`);
      return response;
    }

    if (foundUser.tokenActivate == credentials.token) {
      foundUser.emailVerified = true;
      await this.userRepository.update(foundUser);

      response.user = foundUser;
      response.response = new BaseResponse(1, "Account Verified");
      return response;
    } else {
      response.response = new BaseResponse(-1, "Invalid token. Couldn't verify account");
      return response;
    }

    return response;
  }
/**
 * Get User by ID
 */
  @get('/users/getUser/{userId}', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: UserResponse } },
      },
    },
  })
  async getUser(
    @param.path.number('userId') id: number
  ): Promise<UserResponse> {

    var response: UserResponse = new UserResponse();

    const currentUser = await await this.userRepository.findById(id);

    if (!currentUser) {
      response.response = new BaseResponse(-2, `User not found.`);
      return response;
    }
    response.user = currentUser;

    return response;
  }

/**
 * 
 */

  @get('/users/getUserList/{userId}', {
    responses: {
      '200': {
        description: "Array of User's References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': UserListEntity } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getUserList(@param.path.number('userId') userId: number): Promise<UserListEntity[]> {

    var response: UserResponse = new UserResponse();

    const userList = await await this.userRepository.find({ where: { emailVerified: true, id: { neq: userId } } });

    var userArr: Array<UserListEntity> = [];

    userList.forEach(element => {
      var obj = new UserListEntity();
      obj.email = element.email;
      obj.id = element.id;
      obj.imageUrl = element.imageUrl;
      obj.name = element.name;

      userArr.push(obj);
    });

    return userArr;
  }

/**
 *  Update user details
 */

  @patch('/users/{userId}', {
    responses: {
      '200': {
        description: 'Update user instance',
        content: { 'application/json': { schema: UserResponse } },
      },
    },
  })
  @authenticate('jwt')
  async updateUserDetails(@param.path.number('userId') id: number,
    @requestBody() req: UpdateUserReq,
  ): Promise<UserResponse> {

    var output: UserResponse = new UserResponse();

    const currentUserDetails = await this.userRepository.findById(id);

    if (!currentUserDetails) {
      output.response = new BaseResponse(-1, `User not found.`);
      return output;
    }

    currentUserDetails.imageUrl = req.imageUrl;
    currentUserDetails.name = req.name;

    await this.userRepository.update(currentUserDetails);

    output.user = currentUserDetails;
    output.response = new BaseResponse(1, "User Updated");
    return output;
  }

  /**
   * Change Password  
   */

  @patch('/users/{userId}/changePassword', {
    responses: {
      '200': {
        description: 'Change Password',
        content: { 'application/json': { schema: User } },
      },
    },
  })
  @authenticate('jwt')
  async changePassword(@param.path.number('userId') id: number,
    @requestBody() user: ChangePasswordReq
  ): Promise<BaseResponse> {

    var response: BaseResponse;

    const currentUser = await this.userRepository.findOne({ where: { id: id } });

    const currentPasswordMatched = await this.passwordHahser.comparePassword(
      user.currentPassword,
      currentUser!.password,
    );

    if (!currentPasswordMatched) {
      response = new BaseResponse(-1, 'Current Password is incorrect');
      return response;
    }

    if (user.currentPassword === user.newPassword) {
      response = new BaseResponse(-2, 'new password is same as current password');
      return response;
    }

    currentUser!.password = await this.passwordHahser.hashPassword(user.newPassword);

    await this.userRepository.update(currentUser!);

    response = new BaseResponse(1, "Password Changed");
    return response;
  }

  /**
   * Forget Password request
   */

  @post('/users/forgetPassword', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: { 'x-ts-type': BaseResponse } } },
      },
    },
  })
  async forgotPassword(
    @requestBody() req: ForgetPasswordReq): Promise<BaseResponse> {

    var response: BaseResponse;

    const foundUser = await this.userRepository.findOne({
      where: { email: req.email },
    });

    if (!foundUser) {
      response = new BaseResponse(-1, `User with email ${req.email} not found.`);
      return response;
    }

    if (!foundUser.emailVerified) {
      response = new BaseResponse(-2, `User account with email ${req.email} not verified.`);
      return response;
    }

    foundUser.tokenForget = randomToken();

    await this.userRepository.update(foundUser);

    var content = `Your code is ${foundUser.tokenForget}`;
    sendEmail(foundUser.email, content, 'Reset your password token');

    response = new BaseResponse(1, "Check your email to reset your password");
    return response;

  }

/**
 * Verify Forget Password
 */

  @post('/users/verifyForgetPassword', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: { 'x-ts-type': UserResponse } } },
      },
    },
  })
  async verifyForgotPassword(
    @requestBody() req: VerifyForgetPasswordReq): Promise<UserResponse> {

    var response: UserResponse = new UserResponse();

    const foundUser = await this.userRepository.findOne({
      where: { email: req.email },
    });

    if (!foundUser) {
      response.response = new BaseResponse(-1, `User with email ${req.email} not found.`);
      return response;
    }

    if (foundUser.tokenForget != req.token) {
      response.response = new BaseResponse(-2, `Invalid Token`);
      return response;
    }

    foundUser.password = await this.passwordHahser.hashPassword(req.password);

    await this.userRepository.update(foundUser);

    response.user = foundUser;
    response.response = new BaseResponse(1, "Password reset successful");
    return response;

  }

}
