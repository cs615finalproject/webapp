import {
  CountSchema,
  repository,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  del,
  requestBody,
} from '@loopback/rest';
import { SharedLibrary, Library } from '../models';
import { UserRepository, SharedLibraryRepository, LibraryRepository } from '../repositories';
import { ShareLibraryReq } from '../request/share-library';
import { BaseResponse } from '../response/base-response';
import { SharedLibraryListResponse } from '../response/shared-library-list-response';
import { ShareMultipleLibraryReq } from '../request/share-multiple-library.';
import { SharedLibraryDetail } from '../response/shared-library-detail-response';
import { authenticate } from '@loopback/authentication';


/**
 * Controller for User's Shared Libraries
 */

export class UserSharedLibrariesController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(LibraryRepository)
    public libraryRepository: LibraryRepository,
    @repository(SharedLibraryRepository)
    public sharedLibraryRepository: SharedLibraryRepository,
  ) { }

  /**
   * Getting all shared libraries of given user 
   */
  @post('/users/{userId}/sharedlibrary', {
    responses: {
      '200': {
        description: 'User model instance',
        content: { 'application/json': { schema: { 'x-ts-type': BaseResponse } } },
      },
    },
  })
  @authenticate('jwt')
  async shareLibrary(
    @param.path.string('userId') userId: number,
    @requestBody() shareLibReq: ShareMultipleLibraryReq)
    : Promise<BaseResponse> {

    let response: BaseResponse;

    var arr = shareLibReq.sharedUserIds.split(",");
    var arrUser = arr.map(e => +e);

    arr = shareLibReq.libraryIds.split(",");
    var arrLib = arr.map(e => +e);

    for (const eleUser of arrUser) {
      for (const eleLib of arrLib) {
        const count = await this.sharedLibraryRepository.count({ "and": [{ libraryId: eleLib }, { sharedUserId: eleUser }] });
        if (count.count == 0) {

          var sharedLib: SharedLibrary = new SharedLibrary();
          sharedLib.libraryId = eleLib;
          sharedLib.sharedUserId = eleUser;
          sharedLib.userId = userId;

          await this.sharedLibraryRepository.create(sharedLib);
        }
      }
    }

    response = new BaseResponse(1, "Library shared successfully");

    return response;
  }

  /**
   * Getting specific library by library id from shared libraries 
   */

  @authenticate('jwt')
  @get('/users/{userId}/sharedlibrary/{libraryId}', {
    responses: {
      '200': {
        description: 'User model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async getSharedLibrary(
    @param.path.string('userId') userId: number,
    @param.path.string('libraryId') libraryId: number,
  ): Promise<SharedLibraryListResponse> {

    let response: SharedLibraryListResponse = new SharedLibraryListResponse();

    const sharedUsers = await this.userRepository.sharedlibraries(userId).find({ where: { libraryId: libraryId } });

    if (!sharedUsers) {
      response.response = new BaseResponse(-1, "Library not shared");
      return response;
    }

    response.sharedLibrary = sharedUsers;
    return response
  }


  @get('/users/{userId}/sharedlibraries', {
    responses: {
      '200': {
        description: "Array of User's Libraries",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': SharedLibraryDetail } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getSharedLibraries(
    @param.path.string('userId') userId: number,
  ): Promise<SharedLibraryDetail[]> {
    var response: SharedLibraryDetail[] = [];
    var libs = await this.userRepository.sharedlibraries(userId).find();

    var count = 0;
    for (const element of libs) {
      response.push(new SharedLibraryDetail(element));
      var t = await this.userRepository.findById(element.sharedUserId);
      response[count].sharedUserName = t.name;
      response[count].libraryName = (await this.libraryRepository.findById(element.libraryId)).name;
      count++;
    }


    return response;
  }
/**
 * Getting libraries with shared users
 */

  @get('/users/{userId}/sharedlibrariesWithUser', {
    responses: {
      '200': {
        description: "Array of User's Libraries",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': SharedLibraryDetail } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getSharedLibrariesWithUser(
    @param.path.string('userId') userId: number,
  ): Promise<SharedLibraryDetail[]> {
    var response: SharedLibraryDetail[] = [];
    var libs = await this.sharedLibraryRepository.find({ where: { sharedUserId: userId } });

    var count = 0;
    for (const element of libs) {
      response.push(new SharedLibraryDetail(element));
      var t = await this.userRepository.findById(element.userId);
      response[count].sharedUserName = t.name;
      response[count].libraryName = (await this.libraryRepository.findById(element.libraryId)).name;
      count++;
    }


    return response;
  }

  /**
   * Deleting a shared library 
   */
  @del('/users/{userId}/sharedlibrary', {
    responses: {
      '204': {
        description: 'User DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async UnshareLibrary(
    @param.path.string('userId') userId: number,
    @requestBody() shareLibReq: ShareLibraryReq): Promise<BaseResponse> {

    let response: BaseResponse;
    const library = await this.userRepository.sharedlibraries(userId).find({ where: { id: shareLibReq.libraryId } });
    if (!library) {
      response = new BaseResponse(-1, "library not found");
      return response;
    }

    const res = await this.userRepository.sharedlibraries(userId).delete({ "or": [{ id: shareLibReq.libraryId }, { sharedUserId: shareLibReq.sharedUserId }] })

    console.log(res)
    response = new BaseResponse(1, "Library unshared successfully")
    return response;
  }

/**
 * Unsharing library from shared users 
 */
  @del('/unshare/{libraryId}/users/{userIds}', {
    responses: {
      '204': {
        description: 'Unshare library with multiple users',
      },
    },
  })
  @authenticate('jwt')
  async UnshareLibraryMultipleUser(
    @param.path.string('userIds') userIds: string,
    @param.path.string('libraryId') libraryId: number, ): Promise<BaseResponse> {

    let response: BaseResponse;

    var arr = userIds.split(",");
    var arrs = arr.map(e => +e);

    const res = await this.sharedLibraryRepository.deleteAll({ and: [{ sharedUserId: { inq: arrs } }, { libraryId: libraryId }] });
    console.log(res)
    response = new BaseResponse(1, "Library unshared successfully")
    return response;
  }



}
