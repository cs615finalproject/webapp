import { repository, Filter, Where, Count } from '@loopback/repository';
import { UserRepository, LibraryRepository, ReferenceRepository, SharedLibraryRepository } from '../repositories';
import {
  post,
  get,
  patch,
  del,
  param,
  requestBody,
  HttpErrors,
} from '@loopback/rest';
import { Library, Reference } from '../models';
import { LibraryResponse } from '../response/library-response';
import { CreateLibraryReq } from '../request/create-library';
import { BaseResponse } from '../response/base-response';
import { ReferenceListResponse } from '../response/reference-list-response';
import { authenticate } from '@loopback/authentication';

/**
 * Controller for User's Library
 */
export class UserLibraryController {
  constructor(@repository(UserRepository) protected userRepo: UserRepository,
    @repository(SharedLibraryRepository) protected sharedLibraryRepo: SharedLibraryRepository,
    @repository(LibraryRepository)
    public libraryRepository: LibraryRepository, @repository(ReferenceRepository)
    public referenceRepository: ReferenceRepository) { }

  /**
   * Create or update the library for a given user
   */
  @post('/users/{userId}/library', {
    responses: {
      '200': {
        description: 'User.Library model instance',
        content: { 'application/json': { 'x-ts-type': LibraryResponse } },
      },
    },
  })
  @authenticate('jwt')
  async createLibrary(
    @param.path.string('userId') userId: number,
    @requestBody() req: CreateLibraryReq,
  ): Promise<LibraryResponse> {
    var output: LibraryResponse = new LibraryResponse();

    var library: Library = new Library(req);
    library.type = "Other";

    var libC = await this.userRepo.libraries(userId).find({ where: { isActive: true } });

    if (libC == null) {
      library.isActive = true;
    }

    output.library = await this.userRepo.libraries(userId).create(library);
    output.response = new BaseResponse(1, 'Library Created');
    return output;
  }

  /**
   * Getting all libraries of particular user 
   */

  @get('/users/{userId}/library', {
    responses: {
      '200': {
        description: "Array of User's Libraries",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Library } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getLibraries(
    @param.path.string('userId') userId: number,
  ): Promise<Library[]> {
    const libraries = await this.userRepo
      .libraries(userId).find({ where: { "and": [{ isDeleted: false }, { canBeDeleted: true }] } });
    return libraries;
  }


  /**
   * Getting active library
   */

  @get('/users/{userId}/activeLibrary', {
    responses: {
      '200': {
        description: "User's Active Libraries",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Library } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getActiveLibrary(
    @param.path.string('userId') userId: number,
  ): Promise<Library> {
    const libraries = await this.userRepo
      .libraries(userId).find({ where: { "and": [{ isDeleted: false }, { isActive: true }] }, limit: 1 });
    return (libraries != null) ? libraries[0] : libraries;
  }


  /**
   * Getting all references of active library
   */

  @get('/users/{userId}/activeLibraryReferences', {
    responses: {
      '200': {
        description: "Array of User's References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': ReferenceListResponse } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getActiveReferences(
    @param.path.string('userId') userId: number,
  ): Promise<ReferenceListResponse> {
    var res = new ReferenceListResponse();
    const libraries = await this.userRepo
      .libraries(userId).find({ where: { "and": [{ isDeleted: false }, { isActive: true }] }, limit: 1 });
    var activeLib = (libraries != null) ? libraries[0] : libraries;

    if (activeLib != null) {
      res.libraryName = activeLib.name;
      res.libraryID = activeLib.id;
      res.response = new BaseResponse(1, 'Sussess');
      res.reference = await this.libraryRepository.references(activeLib.id).find();
      return res;
    } else {
      res.response = new BaseResponse(-1, 'no actice library');
      return res;
    }

  }

/**
 * Getting all references from trash library
 */

  @get('/users/{userId}/trashLibraryReferences', {
    responses: {
      '200': {
        description: "Array of Trash References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Reference } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getTrashReferences(
    @param.path.string('userId') userId: number,
  ): Promise<Reference[]> {

    const libraries = await this.userRepo
      .libraries(userId).find({ where: { type: 'Trash' } });
    var trashLib = (libraries != null) ? libraries[0] : libraries;

    return this.libraryRepository.references(trashLib.id).find();

  }


 /**
  * Getting references from Unfiled library
  */

  @get('/users/{userId}/unfiledLibraryReferences', {
    responses: {
      '200': {
        description: "Array of Unfiled References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Reference } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getUnfiledReferences(
    @param.path.string('userId') userId: number,
  ): Promise<Reference[]> {

    const libraries = await this.userRepo
      .libraries(userId).find({ where: { "and": [{ canBeDeleted: false }, { type: 'Unfiled' }] }, limit: 1 });
    var unfiledLib = (libraries != null) ? libraries[0] : libraries;

    return this.libraryRepository.references(unfiledLib.id).find();

  }


  /**
   * Getting deleted libraries
   */

  @get('/users/{userId}/deletedLibrary', {
    responses: {
      '200': {
        description: "Array of User's Libraries",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Library } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getDeletedLibraries(
    @param.path.string('userId') userId: number,
  ): Promise<Library[]> {
    const libraries = await this.userRepo
      .libraries(userId).find({ where: { isDeleted: true } });
    return libraries;
  }


  /**
   * Updating Library  
   */
  @patch('/users/{userId}/libraries/{libraryId}', {
    responses: {
      '200': {
        description: 'User.Library PATCH success count',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                count: 'number',
              },
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async patchLibraries(
    @param.path.string('userId') userId: number,
    @param.path.string('libraryId') libraryId: number,
    @requestBody() library: Partial<Library>,
  ): Promise<Count> {
    return await this.userRepo
      .libraries(userId)
      .patch(library, { id: libraryId });
  }


/**
 * Marking a library as active library  
 */

  @patch('/users/{userId}/makeActive/{libraryId}', {
    responses: {
      '200': {
        description: 'User.Library PATCH success count',
        content: { 'application/json': { 'x-ts-type': BaseResponse } },
      },
    },
  })
  @authenticate('jwt')
  async changeActiveLibrary(
    @param.path.string('userId') userId: number,
    @param.path.string('libraryId') libraryId: number,
  ): Promise<BaseResponse> {
    var library: Library = new Library();
    library.isActive = false;
    await this.userRepo
      .libraries(userId)
      .patch(library);

    library.isActive = true;

    await this.userRepo
      .libraries(userId)
      .patch(library, { id: libraryId });

    return new BaseResponse(1, 'Library Activated');
  }

/**
 * Moving a library to trash
 */

  @del('/users/{userId}/library/{libraryId}/trash', {
    responses: {
      '200': {
        description: 'User.Library DELETE success count',
        content: { 'application/json': { 'x-ts-type': BaseResponse } },
      },
    },
  })
  @authenticate('jwt')
  async deleteLibraryAndTrash(
    @param.path.string('userId') userId: number,
    @param.path.string('libraryId') libraryId: number,
  ): Promise<BaseResponse> {

    const libraries = await this.userRepo
      .libraries(userId).find({ where: { type: "Trash" }, limit: 1 });
    var trashLib = (libraries != null) ? libraries[0] : libraries;

    var patchRef = new Reference();
    patchRef.libraryId = trashLib.id;

    await this.referenceRepository.updateAll(patchRef, { libraryId: libraryId });

    await this.libraryRepository.deleteById(libraryId);

    await this.sharedLibraryRepo.deleteAll({ libraryId: libraryId });

    return new BaseResponse(1, 'Library Deleted');
  }


  /**
   * Deleting a library and removing it from shared libraries 
   */
  
  @del('/users/{userId}/library/{libraryId}/unfiled', {
    responses: {
      '200': {
        description: 'User.Library DELETE success count',
        content: { 'application/json': { 'x-ts-type': BaseResponse } },
      },
    },
  })
  @authenticate('jwt')
  async deleteLibraryAndUnfiled(
    @param.path.string('userId') userId: number,
    @param.path.string('libraryId') libraryId: number,
  ): Promise<BaseResponse> {

    const libraries = await this.userRepo
      .libraries(userId).find({ where: { type: 'Unfiled' }, limit: 1 });
    var unfiledLib = (libraries != null) ? libraries[0] : libraries;

    var patchRef = new Reference();
    patchRef.libraryId = unfiledLib.id;

    await this.referenceRepository.updateAll(patchRef, { libraryId: libraryId })

    await this.libraryRepository.deleteById(libraryId);

    await this.sharedLibraryRepo.deleteAll({ libraryId: libraryId });

    return new BaseResponse(1, 'Library Deleted');
  }
}

