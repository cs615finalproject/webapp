import {
  repository,
} from '@loopback/repository';
import {
  get,
} from '@loopback/rest';
import { EntryType } from '../models';
import { EntryTypeRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class EntryTypeController {
  constructor(
    @repository(EntryTypeRepository)
    public entryTypeRepository: EntryTypeRepository,
  ) { }


  @get('/entry-types', {
    responses: {
      '200': {
        description: 'Array of EntryType model instances',
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': EntryType } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async find(
  ): Promise<EntryType[]> {
    return await this.entryTypeRepository.find();
  }


}
