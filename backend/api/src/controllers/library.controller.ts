import {
  CountSchema,
  repository,
} from '@loopback/repository';
import {
  param,
  patch,
  del,
  requestBody,
} from '@loopback/rest';
import { LibraryRepository, ReferenceRepository } from '../repositories';
import { BaseResponse } from '../response/base-response';
import { RenameLibrary } from '../request/rename-library';
import { LibraryResponse } from '../response/library-response';
import { authenticate } from '@loopback/authentication';

/**
 * Controller for Libraries
 */
export class LibraryController {
  constructor(
    @repository(LibraryRepository)
    public libraryRepository: LibraryRepository,
    @repository(ReferenceRepository)
    public referenceRepository: ReferenceRepository,
  ) { }

/**
 * Renaming library 
 */

  @patch('/libraries/{libraryId}/rename', {
    responses: {
      '200': {
        description: 'User.Library PATCH success count',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                count: 'number',
              },
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async renameLibrary(
    @param.path.string('libraryId') libraryId: number,
    @requestBody() library: RenameLibrary,
  ): Promise<LibraryResponse> {

    var libraryResponse: LibraryResponse = new LibraryResponse();

    const selectedLibrary = await this.libraryRepository
      .findOne({ where: { id: libraryId } });

    if (!selectedLibrary) {
      libraryResponse.response = new BaseResponse(-1, "No library found");
      return libraryResponse;
    }


    if (selectedLibrary.type == "Trash" || selectedLibrary.type == "Unfiled") {
      libraryResponse.response = new BaseResponse(-1, "This library can not be renamed");
      return libraryResponse;
    }

    selectedLibrary.name = library.name;

    await this.libraryRepository
      .update(selectedLibrary);

    libraryResponse.response = new BaseResponse(1, "Library renamed successfully");
    libraryResponse.library = selectedLibrary;

    return libraryResponse;
  }


  /**
   * Deleting libraries
   */
  
  @del('/libraries/{ids}', {
    responses: {
      '204': {
        description: 'Library DELETE success',
      },
    },
  })
  @authenticate('jwt')
  async deleteByIds(@param.path.string('ids') ids: string): Promise<void> {
    var arr = ids.split(",");
    var arrs = arr.map(e => +e);
    arrs.forEach(element => {
      this.libraryRepository.deleteById(element);
    });

    return;
  }
}
