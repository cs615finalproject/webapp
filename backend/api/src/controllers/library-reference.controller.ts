import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  WhereBuilder,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Reference } from '../models';
import { ReferenceRepository, UserRepository, LibraryRepository } from '../repositories';
import { CreateReferenceReq } from '../request/create-reference';
import { ReferenceResponse } from '../response/reference-response';
import { BaseResponse } from '../response/base-response';
import { authenticate } from '@loopback/authentication';

/**
 * Controller for library's references
 */

export class LibraryReferenceController {
  constructor(
    @repository(LibraryRepository)
    public libraryRepository: LibraryRepository,
    @repository(UserRepository)
    public userRepository: UserRepository,
    @repository(ReferenceRepository)
    public referenceRepository: ReferenceRepository,
  ) { }

  /**
   * Creating reference in a library
  */
  @post('/library/{libraryid}/references', {
    responses: {
      '200': {
        description: 'Reference model instance',
        content: { 'application/json': { schema: { 'x-ts-type': ReferenceResponse } } },
      },
    },
  })
  @authenticate('jwt')
  async createReference(
    @param.path.string('libraryid') libraryId: number,
    @requestBody() req: CreateReferenceReq,
  ): Promise<ReferenceResponse> {
    var refResponse: ReferenceResponse = new ReferenceResponse();
    var ref: Reference = new Reference(req);
    refResponse.reference = await this.libraryRepository.references(libraryId).create(ref);
    refResponse.response = new BaseResponse(1, 'Library Created');
    return refResponse;
  }

/**
 * Getting all references of given library by library id
 */
  @get('/library/{libraryid}/references', {
    responses: {
      '200': {
        description: "Array of User's References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Reference } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getReference(
    @param.path.string('libraryid') libraryId: number,
  ): Promise<Reference[]> {
    return await this.libraryRepository.references(libraryId).find();
  }


  /**
   * Getting references from a particular library by given search criteria
  */
  @get('/library/{libraryid}/references/{search}', {
    responses: {
      '200': {
        description: "Array of User's References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Reference } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getReferenceSearch(
    @param.path.string('libraryid') libraryId: number,
    @param.path.string('search') search: string,
  ): Promise<Reference[]> {
    var pattern = new RegExp('.*' + search + '.*', "i") + '';
    return await this.libraryRepository.references(libraryId).find({ where: { or: [{ title: { like: search } }, { author: { like: pattern } }, { editor: { like: pattern } }, { bookTitle: { like: pattern } }, { journal: { like: search } }, { publisher: { like: pattern } }] } });
  }


  /**
   * Getting references from all libraries of a particular user by given search criteria
   */
  
  @get('/user/{userId}/references/{search}', {
    responses: {
      '200': {
        description: "Array of User's References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Reference } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getAllReferenceSearch(
    @param.path.string('userId') userId: number,
    @param.path.string('search') search: string,
  ): Promise<Reference[]> {
    var lib = await this.userRepository.libraries(userId).find({ where: { isDeleted: false } });

    var libId = lib.map(e => e.id);
    var pattern = new RegExp('.*' + search + '.*', "i") + '';
    return await this.referenceRepository.find({ where: { and: [{ libraryId: { inq: libId } }, { or: [{ title: { like: search } }, { author: { like: pattern } }, { editor: { like: pattern } }, { bookTitle: { like: pattern } }, { journal: { like: search } }, { publisher: { like: pattern } }] }] } });

  }

  /**
   * Geting all references of a specific user
   */

  @get('/user/{userId}/allreferences/', {
    responses: {
      '200': {
        description: "Array of User's References",
        content: {
          'application/json': {
            schema: { type: 'array', items: { 'x-ts-type': Reference } },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async getAllUserReference(
    @param.path.string('userId') userId: number,
  ): Promise<Reference[]> {
    var lib = await this.userRepository.libraries(userId).find({ where: { and: [{ isDeleted: false }, { canBeDeleted: true }] } });

    var libId = lib.map(e => e.id);
    return await this.referenceRepository.find({ where: { libraryId: { inq: libId } } });

  }

  /**
   * Getting specific reference by refernce id from a given library
   */

  @patch('/library/{libraryId}/references/{refId}', {
    responses: {
      '200': {
        description: 'Reference PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async update(
    @param.path.string('libraryId') libraryId: number,
    @param.path.string('refId') refId: number,
    @requestBody() reference: CreateReferenceReq,
  ): Promise<BaseResponse> {

    var refResponse: BaseResponse
    const currentref = await this.libraryRepository.references(libraryId).find({ where: { id: refId } });

    if (!currentref) {
      refResponse = new BaseResponse(-1, "Refernce not found");
      return refResponse;
    }

    var ref = new Reference(reference);
    await this.libraryRepository.references(libraryId).patch(ref, { id: refId });

    refResponse = new BaseResponse(1, "Reference updated successfully")
    return refResponse;
  }

  /**
   * Deleting all references from Trash
   */

  @del('/library/{userId}/emptyTrash', {
    responses: {
      '200': {
        description: 'Reference Delete success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async emptyTrash(
    @param.path.string('userId') userId: number,
  ): Promise<BaseResponse> {

    var refResponse: BaseResponse
    const trash = await this.userRepository.libraries(userId).find({ where: { type: "Trash" } });

    if (!trash[0]) {
      refResponse = new BaseResponse(-1, "Trash is empty");
      return refResponse;
    }

    await this.libraryRepository.references(trash[0].id).delete();

    refResponse = new BaseResponse(1, "trash emptied successfully")
    return refResponse;
  }

/**
 * Deleting multiple references from a given library
 */

  @del('/user/{userId}/library/{libraryId}/referencesMultiple/{ids}', {
    responses: {
      '204': {
        description: 'Library DELETE success',
        content: { 'application/json': { schema: BaseResponse } },
      },
    },
  })
  @authenticate('jwt')
  async deleteByIds(@param.path.string('userId') userId: number,
    @param.path.string('libraryId') libraryId: number,
    @param.path.string('ids') ids: string): Promise<BaseResponse> {

    var arr = ids.split(",");
    var arrs = arr.map(e => +e);


    const trashLib = await this.libraryRepository.findOne({ where: { and: [{ type: "Trash" }, { userId: userId }] } });

    var patchRef = new Reference();
    patchRef.libraryId = trashLib!.id;

    await this.referenceRepository.updateAll(patchRef, { and: [{ libraryId: libraryId }, { id: { inq: arrs } }] });
    return new BaseResponse(1, "Reference(s) Trash");
  }

/**
 * Moving references from one library to another library
 */

  @patch('/library/{fromLib}/{toLib}/referencesMultiple/{ids}', {
    responses: {
      '204': {
        description: 'Library Reference Move success',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async moveMultipleReferences(@param.path.string('fromLib') fromLib: number,
    @param.path.string('toLib') toLib: number,
    @param.path.string('ids') ids: string): Promise<BaseResponse> {

    var arr = ids.split(",");
    var arrs = arr.map(e => +e);

    var ref = new Reference();
    ref.libraryId = toLib;

    await this.referenceRepository.updateAll(ref, { and: [{ id: { inq: arrs } }, { libraryId: fromLib }] })
    return new BaseResponse(1, 'references moved');
  }
}
