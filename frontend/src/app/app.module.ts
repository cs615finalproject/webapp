/**Using angular built-in components-Start */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
/**Using angular built-in components-Stop */

/**Creating own components-Start */
import { SignupComponent } from './signup/signup.component';
import { GetAllLibrariesComponent } from './get-all-libraries/get-all-libraries.component';
import { ListReferencesComponent } from './references/list-references.component';
import { VerifyUserComponent } from './verify-user/verify-user.component';
import { VerifyForgotPasswordComponent } from './verify-forgot-password/verify-forgot-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SigninComponent } from './signin/signin.component';
import { DataService } from './data-service/dataService';
import { TrashLibraryReferencesComponent } from './trash-library-references/trash-library-references.component';
import { UnFiledLibraryReferencesComponent } from './un-filed-library-references/un-filed-library-references.component';
import { LibrariesSharedWithMeComponent } from './libraries-shared-with-me/libraries-shared-with-me.component';
import { LibrariesSharedByMeComponent } from './libraries-shared-by-me/libraries-shared-by-me.component';
import { GetAllReferencesComponent } from './get-all-references/get-all-references.component';
import { ActiveLibrariesComponent } from './active-libraries/active-libraries.component';
import { MyAccountDetailsComponent } from './my-account-details/my-account-details.component';
import { AuthenticationGuard } from './authentication.guard';
/**Creating own componets-End */

/** Using external libraries-start */
import { Ng2SmartTableModule } from 'ng2-smart-table'; /**Referenced from https://github.com/akveo/ng2-smart-table */
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';/** Referenced from https://www.npmjs.com/package/ng-multiselect-dropdown */
import { NgSelectModule } from 'ng-custom-select'; /**Referenced from https://www.npmjs.com/package/ng-custom-select */
/** Using external libraries-End */


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    GetAllLibrariesComponent,
    ListReferencesComponent,
    VerifyUserComponent,
    VerifyForgotPasswordComponent,
    ForgotPasswordComponent,
    TrashLibraryReferencesComponent,
    UnFiledLibraryReferencesComponent,
    LibrariesSharedWithMeComponent,
    LibrariesSharedByMeComponent,
    GetAllReferencesComponent,
    ActiveLibrariesComponent,
    MyAccountDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    Ng2SmartTableModule,
    NgSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],
  exports: [RouterModule],

  entryComponents: [],
  providers: [

    DataService,

    AuthenticationGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
