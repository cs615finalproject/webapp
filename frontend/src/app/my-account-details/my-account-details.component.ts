import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-account-details',
  templateUrl: './my-account-details.component.html',
  styleUrls: ['./my-account-details.component.css']
})
export class MyAccountDetailsComponent implements OnInit {
  userId = JSON.parse(sessionStorage.getItem('userId'));
 userName = JSON.parse(sessionStorage.getItem('userName'));
  message;
  newUserName;
  currentPassword;
  newPassword;
  changePasswordResponseCode;
  userInfoMessage;
  email = JSON.parse(sessionStorage.getItem('userEmail'));
  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {

  }

/**
 * Method to create the url as required by the api to change the username by passing the 
 * logged in user id. Also, creatin g the patchData by assigning newUserName according to the user input.
 * 
 * @return void
 */
  changeUserName() {
    let url = `${environment.app_url}/users/${this.userId}`;

    let patchData =
    {
      name: this.newUserName,
      imageUrl: ""
    }

    this.dataService.patchData(url, patchData).subscribe(data => {
      this.message = data['response'].message;
      console.log(data);
      sessionStorage.setItem('userName', JSON.stringify(this.newUserName));
      if (1 == data['response'].code) {
        //Changing the username so that is reflected in the html page
        this.userName = this.newUserName;
      }
      this.newUserName = "";

    }
    );

  }

  /**
 * Method to create the url as required by the api to change the password by passing the 
 * logged in user id. Also, creating the patchData by assigning currentPassword and new password
 *  according to the user input.
 * 
 * @return void
 */
  changePassword() {
    let url = `${environment.app_url}/users/${this.userId}/changePassword`;

    let patchData =
    {
      currentPassword: this.currentPassword,
      newPassword: this.newPassword
    }

    this.dataService.patchData(url, patchData).subscribe(data => {
      //Setting the message to diaply it to the user
      this.message = data['message'];
      console.log(data);

      this.currentPassword = "";
      this.newPassword = "";
      this.changePasswordResponseCode = data['code'];
      if (1 == this.changePasswordResponseCode)
      {
        //In case change password is successful, inform the user that he/she will have to login again
        this.userInfoMessage="Please login again with new password";
      }

    }
    );

  }

   /**
 * Method to navigate to the signin page
 * when the change password is successful or user clicks on ok button of the popup after seein g the message
 * 
 * @return void
 */
  navigate() {
    if (1 == this.changePasswordResponseCode) {

      sessionStorage.clear();
      this.router.navigate(['signin']);
    }

  }

}
