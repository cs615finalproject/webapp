import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetAllReferencesComponent } from './get-all-references.component';

describe('GetAllReferencesComponent', () => {
  let component: GetAllReferencesComponent;
  let fixture: ComponentFixture<GetAllReferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetAllReferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetAllReferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
