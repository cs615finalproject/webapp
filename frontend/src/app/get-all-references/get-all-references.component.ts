import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';

import { DatePipe } from '@angular/common';

declare const $: any;

@Component({
  selector: 'app-get-all-references',
  templateUrl: './get-all-references.component.html',
  styleUrls: ['./get-all-references.component.css']
})
export class GetAllReferencesComponent implements OnInit {


  userId = sessionStorage.getItem('userId');

  entryTypes = [{ id: 0, name: 'Article' }, { id: 1, name: 'Book' }, { id: 2, name: 'InProceedings' }, { id: 3, name: 'InCollection' }];

  /** 
  * Initializing ng2-smart table settings with the required column
  */
  settings = {

    columns: {
      entryType: {
        title: 'Entry Type',
        filter: true,
        valuePrepareFunction: (entryType) => {
          if (entryType !== undefined && entryType !== null) {
            if (entryType instanceof Array) {
              return entryType[0].id;

            }

            else {
              return this.entryMapping[entryType];
            }
          }
          else {
            return null;
          }
        },

      },
      author: {
        title: 'Author',
        filter: true
      },
      bookTitle: {
        title: 'Book Title',
        filter: true
      },
      editor: {
        title: 'Editor',
        filter: true
      },
      title: {
        title: 'Title',
        filter: true
      },
      journal: {
        title: 'Journal',
        filter: true
      },
      publisher: {
        title: 'Publisher',
        filter: true
      },
      year: {
        title: 'Year',
        filter: true
      },
      volume: {
        title: 'Volume',
        filter: true
      },
      dateCreated: {
        title: 'Date Created',
        type: 'date',
        valuePrepareFunction: (date) => {
          if (date) {
            return new DatePipe('en-GB').transform(date, 'dd/MM/yyyy hh:mm');
          }
          return null;
        },

      }
    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,

    },
    attr: {
      class: 'table table-bordered'
    }
  };




  rows;


  rowIndices = {};
  entryMapping = ['Article', 'Book', 'InProceedings', 'InCollection'];
  private sub: any;

  constructor(private route: ActivatedRoute, private dataService: DataService) {

    //Calling getData method of dataService by passing the url to get all the references of thbe logged in user
    let url = `${environment.app_url}/user/${this.userId}/allreferences`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data)
      this.rows = data;

    });
  }



  ngOnInit() {

    //jquery to close the card everytime the user clicks on the close button.
    $('.boxs-close').on('click', function () {
      $(this).parents('.card').addClass('closed').fadeOut();
    });


  }







}

