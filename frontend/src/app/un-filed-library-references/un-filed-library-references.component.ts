import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DatePipe } from '@angular/common';

declare const $: any;

@Component({
  selector: 'app-un-filed-library-references',
  templateUrl: './un-filed-library-references.component.html',
  styleUrls: ['./un-filed-library-references.component.css']
})
export class UnFiledLibraryReferencesComponent implements OnInit {
  userId = sessionStorage.getItem('userId');
  // initializing the settings of ng2-smart table by assigning the columns
  settings = {

    columns: {
      entryType: {
        title: 'Entry Type',
        filter: true
      },
      author: {
        title: 'Author',
        filter: true
      },
      bookTitle: {
        title: 'Book Title',
        filter: true
      },
      editor: {
        title: 'Editor',
        filter: true
      },
      title: {
        title: 'Title',
        filter: true
      },
      journal: {
        title: 'Journal',
        filter: true
      },
      publisher: {
        title: 'Publisher',
        filter: true
      },
      year: {
        title: 'Year',
        filter: true
      },
      volume: {
        title: 'Volume',
        filter: true
      },
      dateCreated: {
        title: 'Date Created',
        type: 'date',
        //Using Data pipe to get the date in proper format
        valuePrepareFunction: (date) => {
          if (date) {
            return new DatePipe('en-GB').transform(date, 'dd/MM/yyyy hh:mm');
          }
          return null;
        },

      }
    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,

    },
    attr: {
      class: 'table table-bordered'
    }
  };


  rows;

  constructor(private route: ActivatedRoute, private dataService: DataService, private http: HttpClient) {
    this.getUnfiledLibraryreferences();
  }

  /**
   * Method to get all the references of the unfiled library
   * by passing the url as required by the api and calling the getData method of dataService
   *  
   * @returns void
   */
  getUnfiledLibraryreferences() {
    let url = `${environment.app_url}/users/${this.userId}/unfiledLibraryReferences`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data)

      this.rows = data;
    });
  }





  ngOnInit() {

    //jquery toremove the card when the user clicks on close of the section card
    $('.boxs-close').on('click', function () {
      $(this).parents('.card').addClass('closed').fadeOut();
    });


  }






  message;



}

