import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnFiledLibraryReferencesComponent } from './un-filed-library-references.component';

describe('UnFiledLibraryReferencesComponent', () => {
  let component: UnFiledLibraryReferencesComponent;
  let fixture: ComponentFixture<UnFiledLibraryReferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnFiledLibraryReferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnFiledLibraryReferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
