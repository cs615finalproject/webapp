import { Component, OnInit, Renderer2 } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { DataService } from './data-service/dataService';
import { environment } from 'src/environments/environment';
import { stringify } from '@angular/core/src/util';

declare const $: any;
declare const jquery: any;
declare const screenfull: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


    userId: number;

    isLoggedIn = false;


    constructor(private renderer: Renderer2, private router: Router, private dataService: DataService) {
        //Everytime check if the user is logged in or not so as as to put it in the ngIf condition of the html elements

        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationStart) {
                    this.isLoggedIn = dataService.isLoggedIn();
                }
            });

    }
    /**
     * Method to delete the session when the user clicks on logOut.
     */
    deleteSession() {
        this.isLoggedIn = false;
        sessionStorage.clear();
    }

    ngOnInit() {
       
    }
}