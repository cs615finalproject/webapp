/**
 * Authentication guard service to check whether the user is logged in or not.
 * If the user is not logged in, it navigates to the signin component
 */
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from './data-service/dataService';
import {NavigationStart, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(private dataService:DataService, private router:Router)
  {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(!this.dataService.isLoggedIn())
    {
      //
      this.router.navigate(['/signin']);
      return false;
    }
    return true;
  }
  
}
