import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';

import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare const $: any;

@Component({
  selector: 'app-get-all-libraries',
  templateUrl: './get-all-libraries.component.html',

  styles: [`
  @media screen and (max-width: 800px) {
    .desktop-hidden {
      display: initial;
    }
    .mobile-hidden {
      display: none;
    }
  }
  @media screen and (min-width: 800px) {
    .desktop-hidden {
      display: none;
    }
    .mobile-hidden {
      display: initial;
    }
  }
`],
  styleUrls: ['./get-all-libraries.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GetAllLibrariesComponent implements OnInit {
  libraryName;
  dropdownList;
  selectedItems;
  dropdownSettings = {};
  userId = sessionStorage.getItem('userId');
  selectedLibraryForDeletion = { "key": "unfiled", "value": 2 };

  //Initializing styleGuide for multiselect dropdown
  styleGuide = {
    caretClass: 'caret',
    selectBoxClass: 'dropdown-wrapper',
    selectMenuClass: 'dropdown',
    optionsClass: 'option'
  };


  /** 
   * Initializing ng2-smart table settings with the required columns
   * and setting multiselect option to true.
   * Also, adding custom viewReferences link in the actions column
  */
  settings = {
    selectMode: 'multi',
    columns: {
      name: {
        title: 'Name',
        filter: true
      },
      type: {
        title: 'Type',
        filter: true
      },
      isActive: {
        title: 'IsActive',
      },
      dateCreated: {
        title: 'Date',
        type: 'date',
        valuePrepareFunction: (date) => {
          if (date) {
            return new DatePipe('en-GB').transform(date, 'dd/MM/yyyy hh:mm');
          }
          return null;
        },

      },

    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          name: 'viewReferences',
          title: 'References ',
        }

      ],
    },
    attr: {
      class: 'table table-bordered'
    }
  };

  rowSelected;
  newLibraryName;
  message;
  trashOrUnfiledLibraryForReferences = [{ "key": "trash", "value": 1 },
  { "key": "unfiled", "value": 2 }];
  isLoggedIn = false;

  rows;
  selected = [];
  showRenameModal = false;
  checkedRows;
  checkedRowsId;

  /**
   * Method to navigate to the listReferences page when a user
   * clicks on viewReferences link on a row of the table
   * @param event- action performed by the user
   * @returns void
   */
  onCustom(event) {
    this.router.navigate(['/references', `${event.data.id}`, `${event.data.name}`]);
  }


  /**
   * Method to navigate to the listReferences page when a user
   * clicks on viewReferences link on a row of the table
   * @param event- action performed by the user
   * @returns void
   */
  onUserRowSelect(event) {
    this.checkedRows = event.selected;

  }



  /**
    * Method to change the active library of the logged in user.
    * It passes the id of the selected row to the url and calls patchData method
    * of dataService.
    * Again calls getLibraries method so that isActive column 
    * is reflected as true for the selected library
    * 
    * @param event- action performed by the user
    * @returns void
    */
  changeActiveLibrary() {

    let url = `${environment.app_url}/users/${this.userId}/makeActive/${this.checkedRows[0].id}`;
    this.dataService.patchData(url, {}).subscribe(data => {
      console.log(data)
      if (1 == data['count']) {
        this.message = this.checkedRows[0].name + " is now the active library";
      }
      this.getLibraries();
    });

  }
  constructor(private dataService: DataService, private router: Router, private formBuilder: FormBuilder) {
    this.isLoggedIn = dataService.isLoggedIn();

    this.getLibraries();


  }


  /**
   * Method to get the users List from the database by 
   * calling the getData method of dataService.
   * It passes the logged in user id in the url and 
   * assigns dropdownlist variable to the return data 
   * 
   * @returns void
   */
  getUsersList() {
    let url = `${environment.app_url}/users/getUserList/${this.userId}`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data);
      this.dropdownList = data;
    });
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };


  }


 
  ngOnInit() {

    //jquery to close the card whenever the user clicks on close icon
    $('.boxs-close').on('click', function () {
      $(this).parents('.card').addClass('closed').fadeOut();
    });

  }

   /**
   * Method to get all the libraries of the logged in user by calling
   * getData method of dataService and assigning rows variable to the returned data
   * So that the data is reflected in the table.
   * 
   * @returns void
   */
  getLibraries() {

    let url = `${environment.app_url}/users/${this.userId}/library`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data)
      this.rows = data;

    });

  }



  /**
   * Method to rename a library.
   * It creates patchData by assigning name to the newLibraryName that
   * is binded to the text input.
   * It then calls the patchData method of dataService by passing the url and patchData.
   * Again calls getLibraries method so that the changed name is reflected in the table
   * 
   * 
   * @returns void
   */

  renameLibrary() {
    const patchData =
    {
      name: this.newLibraryName,


    }
    this.dataService.patchData(`${environment.app_url}/libraries/${this.checkedRows[0].id}/rename`, patchData)
      .subscribe(data => {

        console.log(data);
        this.message = data['response'].message;

       
        this.newLibraryName = '';  //So that user sees blank text input everytime the rename button clicked.
      
        this.getLibraries();

      }
      );
  }

   /**
   * Method to share one or more libraries to one or more users.
   * It first converts the checkedRows into comma separated checkedRowsId
   * and selectedItems into comma separated selecteditems Id and then assigns these
   * variables to the postData json and passes it to the postData method of dataService
   * Again calls getLibraries method so that the changed name is reflected in the table
   *  
   * @returns void
   */
  shareLibrary() {
    var checkedRowsId;
    for (var i = 0; i < this.checkedRows.length; i++) {
      if (i == 0 && i != this.checkedRows.length - 1)
        checkedRowsId = this.checkedRows[i].id + ',';
      else if (i == 0 && i == this.checkedRows.length - 1) {
        checkedRowsId = this.checkedRows[i].id;
      }
      else if (i == this.checkedRows.length - 1) {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id;
      }
      else {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id + ',';
      }
    }
    console.log(checkedRowsId);
    var selectedItemsId;
    for (var i = 0; i < this.selectedItems.length; i++) {
      if (i == 0 && i != this.selectedItems.length - 1)
        selectedItemsId = this.selectedItems[i].id + ',';
      else if (i == 0 && i == this.selectedItems.length - 1) {
        selectedItemsId = this.selectedItems[i].id;
      }
      else if (i == this.selectedItems.length - 1) {
        selectedItemsId = selectedItemsId + this.selectedItems[i].id;
      }
      else {
        selectedItemsId = selectedItemsId + this.selectedItems[i].id + ',';
      }
    }

    const postData =
    {
      libraryIds: String(checkedRowsId),
      sharedUserIds: String(selectedItemsId)


    }


    this.dataService.postData(`${environment.app_url}/users/${this.userId}/sharedlibrary`, postData)
      .subscribe(data => {

        console.log(data);
        this.message = data['message'];

        this.selectedItems = [];

     
        this.getLibraries();

      }
      );

  }


   /**
   * Method to delete one or more libraries.
   * Based on selectedLibraryForDeletion variable which is binded the the select box,
   * it calls the api to either move the references to the trash library or unfiled library.
   *  
   * @returns void
   */
  deleteLibrary() {
    var checkedRowsId;
    for (var i = 0; i < this.checkedRows.length; i++) {
      if (i == 0 && i != this.checkedRows.length - 1)
        checkedRowsId = this.checkedRows[i].id + ',';
      else if (i == 0 && i == this.checkedRows.length - 1) {
        checkedRowsId = this.checkedRows[i].id;
      }
      else if (i == this.checkedRows.length - 1) {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id;
      }
      else {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id + ',';
      }
    }
    console.log(checkedRowsId);
    console.log(this.selectedLibraryForDeletion.key);
  this.dataService.deleteData(`${environment.app_url}/users/${this.userId}/library/${checkedRowsId}/${this.selectedLibraryForDeletion.key}`)
      .subscribe(data => {

        console.log(data);
        this.message = data['message'];



        //  let url = `${environment.app_url}/users/${this.userId}/library`;
        // this.userLibraries=this.signInService.getData(url);
        this.getLibraries();

      }
      );
  }

   /**
   * Method to create a new library.
   * It creates postData json by assigning name to the libraryName input by the user.
   * It then calls the postData method of dataService by passing the api url and postData json.
   * It again calls getLibraries so that the newly created library is reflected
   * in the table.
   *  
   * @returns void
   */
  createLibrary() {
    const postData =
    {
      name: this.libraryName,


    }
    this.dataService.postData(`${environment.app_url}/users/${this.userId}/library`, postData)
      .subscribe(data => {

        console.log(data);
        this.message = data['response'].message;
        this.getLibraries();
        this.libraryName = '';

      }
      );

  }



}
