/**
 * Component to show the login page to the user and navigating to the active library page in case 
 * the login is successful, otherwise displaying the error message */
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  email;
  password;
  errorMessage;
  userId;
  
  //Declaring constructor and passing dataservice and router
  constructor(private dataService : DataService,private router:Router) {
   
   }

  ngOnInit() {
   
  }

  /**
   * Method to construct the post data by assigning the user added values
   * and passing the required api url and postData to the postData method of dataService.
   * On Successful login, it sets the session variables to be used by other components and
   * navigates to the active library page.
   * Otherwise, it shows the error message
   * 
   * @returns void
   */
  signin()
  {
    const postData=
    {
      email :this.email,
      password:this.password

    }
    this.dataService.postData(`${environment.app_url}/users/login`,postData)
    .subscribe(data=>
      {
       
        console.log(data['response'].code);
        
        if(1==data['response'].code)
        {
          sessionStorage.setItem('userToken',JSON.stringify(data['token']));
          sessionStorage.setItem('userId',JSON.stringify(data['user'].id));
          sessionStorage.setItem('userName',JSON.stringify(data['user'].name));
          sessionStorage.setItem('userEmail',JSON.stringify(data['user'].email));
          this.userId=data['user'].id;
          this.router.navigate(['library/activeLibrary']);
        }
        else 
        {
          this.errorMessage=data['response'].message;
        }

      });
  }

}
