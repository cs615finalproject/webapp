/**Component to display all the references of a particular library owned by the logged in user
 * and allowing the user to add new references, edit the existing references, Deleting multiple references and moving them to
 * trash library and also moving the references from one library to another.
 */
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DatePipe } from '@angular/common';

declare const $: any;
@Component({
  selector: 'app-list-references',
  templateUrl: './list-references.component.html',
  styleUrls: ['./list-references.component.scss'],
  //Adding style for responsiveness
  styles: [`
    @media screen and (max-width: 576px) {
      .desktop-hidden {
        display: initial;
      }
      .mobile-hidden {
        display: none;
      }
    }
    @media screen and (min-width: 800px) {
      .desktop-hidden {
        display: none;
      }
      .mobile-hidden {
        display: initial;
      }
    }
  `],
})
export class ListReferencesComponent implements OnInit {
  libraries;
  userId = sessionStorage.getItem('userId');
  temp = [];
  id: number;
  name;
  rows;
  selected = [];
  entryType;
  author;
  bookTitle;
  editor;
  title;
  journal;
  publisher;
  year;
  volume;
  selectedLibraryForMovingReferences;
  entryTypes = [{ id: 0, name: 'Article' }, { id: 1, name: 'Book' }, { id: 2, name: 'InProceedings' }, { id: 3, name: 'InCollection' }];
  styleGuide = {
    caretClass: 'caret',
    selectBoxClass: 'dropdown-wrapper',
    selectMenuClass: 'dropdown',
    optionsClass: 'option'
  };
  checkedRows;

  //Initializing the settings of the ng-2 smart table and making it multiselectable table
  settings = {
    selectMode: 'multi',
    columns: {
      entryType: {
        title: 'Entry Type',
        filter: true,
        valuePrepareFunction: (entryType) => {
          if (entryType !== undefined && entryType !== null) {
            if (entryType instanceof Array) {
              return entryType[0].id;

            }

            else {
              return this.entryMapping[entryType];
            }
          }
          else {
            return null;
          }
        },

      },
      author: {
        title: 'Author',
        filter: true
      },
      bookTitle: {
        title: 'Book Title',
        filter: true
      },
      editor: {
        title: 'Editor',
        filter: true
      },
      title: {
        title: 'Title',
        filter: true
      },
      journal: {
        title: 'Journal',
        filter: true
      },
      publisher: {
        title: 'Publisher',
        filter: true
      },
      year: {
        title: 'Year',
        filter: true
      },
      volume: {
        title: 'Volume',
        filter: true
      },
      dateCreated: {
        title: 'Date Created',
        type: 'date',
        valuePrepareFunction: (date) => {
          if (date) {
            return new DatePipe('en-GB').transform(date, 'dd/MM/yyyy hh:mm');
          }
          return null;
        },

      }
    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,

    },
    attr: {
      class: 'table table-bordered'
    }
  };

  /**
   * method to set the checkedRows variable 
   * everytime the user selects a row
   * 
   * @param event-the event performed by the user
   * @returns void
   * 
   */
  onUserRowSelect(event) {
    this.checkedRows = event.selected;
    console.log(this.checkedRows[0]);

  }




  rowIndices = {};
  entryMapping = ['Article', 'Book', 'InProceedings', 'InCollection'];
  private sub: any;
  constructor(private route: ActivatedRoute, private dataService: DataService) {
    // Setting the route parameters passed bhy the get-all-libraries component to id and name
    //So as to use it for all the required operations(adding n ew reference, editing/deleting/moving existing to 
    //another library)
    this.getLibraries();
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['libraryId']; // (+) converts string 'id' to a number
      this.name = params['libraryName'];
      console.log(this.id);
      console.log(this.name);



      this.getReferences();
    });
  }

  /**
   * Method to initialize the dropdown settings of the ng-multiselect dropdown to show the dropdown of all the libraries owned
   * bhy the user when the user selects on moveTo Another Library button
   * 
   * @returns void
   */
  getEntryTypes() {

    this.dropdownSettings = {
      //using ng-multiselect dropdown and making the singleSelection option to true
      singleSelection: true,
      idField: 'Id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };


  }

  config;
  dropdownSettings;
  dropdownList;
  selectedItems;

  /**
   * Method to get all the libraries owned by the logged in user to map it to the dropdown list
   * when user selects move to another library button. So that the user can select a library from the dropdown list
   * to move the selected references to the another library
   */
  getLibraries() {

    let url = `${environment.app_url}/users/${this.userId}/library`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data);
      this.dropdownList = data;

      this.dropdownSettings = {
        singleSelection: true,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true,
        closeDropDownOnSelection: true
      };
      console.info(this.libraries);

    });
  }


  ngOnInit() {

    var self = this;


    $('.boxs-close').on('click', function () {
      $(this).parents('.card').addClass('closed').fadeOut();
    });


  }

  /**
   * Method to add a new reference to the library
   * It creates post data using values entered by the user
   * and calling the postData method of dataservice
   * 
   * @returns void
   */

  createReference() {

    let url = `${environment.app_url}/library/${this.id}/references`;

    let postData =
    {
      entryType: +this.entryType,// + is used to convert the string into number
      author: this.author,
      bookTitle: this.bookTitle,
      editor: this.editor,
      title: this.title,
      journal: this.journal,
      publisher: this.publisher,
      year: String(this.year), //Converting year to string as in html, input type-number was used
      volume: this.volume
    };

    this.dataService.postData(url, postData).subscribe(data => {


      if (1 == data['response'].code) {
        this.message = "New Reference Created";
      }

      //Clearing the fields so that the user doesnot see the previously added/selected data again when he/she tries to add
      //another reference
      this.entryType = null;
      this.author = null;
      this.bookTitle = null;
      this.editor = null;
      this.title = null;
      this.journal = null;
      this.publisher = null;
      this.year = null;
      this.volume = null;

      // Calling getReference to reflect the changes in the table
      this.getReferences();
    });

  }

  /**
   * Generalized method to convert the checkedRows array to the comma separated ids
   * So as to pass it to the api
   * 
   * @returns void
   */
  getCheckedrowsId() {
    var checkedRowsId;
    for (var i = 0; i < this.checkedRows.length; i++) {
      if (i == 0 && i != this.checkedRows.length - 1)
        checkedRowsId = this.checkedRows[i].id + ',';
      else if (i == 0 && i == this.checkedRows.length - 1) {
        checkedRowsId = this.checkedRows[i].id;
      }
      else if (i == this.checkedRows.length - 1) {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id;
      }
      else {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id + ',';
      }
    }
    return checkedRowsId;
  }

  /**
   * Method to move multiple references from one library to another
   * based on the rows selecgted by the user
   * 
   * @returns void
   */
  moveReferencesToAnotherLibrary() {

    let checkedRowsId = this.getCheckedrowsId();


    let url = `${environment.app_url}/library/${this.checkedRows[0].libraryId}/${this.selectedItems[0].id}/referencesMultiple/${checkedRowsId}`;

    this.dataService.patchData(url, {}).subscribe(data => {
      console.log(data)
      // assigning message to display it to the user
      this.message = data['message'];


      this.getReferences();
    });

  }

  /**
  * Method to edit an existing reference.
  * It creates post data by the values as edited by the user
  * and passes it to the postdata method of dataService
  * 
  * @returns void
  */
  editReference() {

    let url = `${environment.app_url}/library/${this.checkedRows[0].libraryId}/references/${this.checkedRows[0].id}`;


    let patchData =
    {

      entryType: +this.checkedRows[0].entryType,
      author: this.checkedRows[0].author,
      bookTitle: this.checkedRows[0].bookTitle,
      editor: this.checkedRows[0].editor,
      title: this.checkedRows[0].title,
      journal: this.checkedRows[0].journal,
      publisher: this.checkedRows[0].publisher,
      year: String(this.checkedRows[0].year), //Converting year to string as in html, input type-number was used
      volume: this.checkedRows[0].volume,

    };

    this.dataService.patchData(url, patchData).subscribe(data => {
      console.log(data)

      //Again calling getReferences method to reflect the changes
      this.getReferences();
    });

  }

/**
 * Method to get all the references of a particular library
 * by calling getData method of dataService
 * 
 * @returns void
 */

  getReferences() {
    let url = `${environment.app_url}/library/${this.id}/references`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data)
      this.rows = data;

    });

  }

  message;

  /**
 * Method to delete multiple references based on the rows selected by the logged in user
 * by calling deleteData method of dataService
 * 
 * @returns void
 */

  deleteReferences() {

    let checkedRowsId = this.getCheckedrowsId();
    let url = `${environment.app_url}/user/${this.userId}/library/${this.id}/referencesMultiple/${checkedRowsId}`;
    this.dataService.deleteData(url).subscribe(data => {
      console.log(data);


      this.getReferences();
    });


  }

}
