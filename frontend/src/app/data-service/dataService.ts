/**
 * Service For making all the APi calls using httpclient
 */

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  userToken;
  httpOptions;


  constructor(private http: HttpClient) {


  }

  /**
   * Method to check whether a user is logged in or not
   * based on the token set in the sessionStorage.
   * @returns void
   */
  isLoggedIn() {
    console.log(sessionStorage.getItem('userToken'));
    if (null === sessionStorage.getItem('userToken')) {
      return false;
    }
    else {
      return true;
    }
  }

  /**
   * Generalized method to add data into database by calling post api based on the url,data and headers in the httpOptions.
   * @param url- url to be passed to api
   * @param data- data parameter to be passed to api
   * @returns data returned by the api.
   */
  postData(url, data) {
    console.log(url, data);
    //Getting token from sessionStorage stored after login successful and setting the authorization of httpheaders to this token
    this.userToken = JSON.parse(sessionStorage.getItem('userToken'));
    this.httpOptions = {
      headers: new HttpHeaders({

        'Authorization': `${this.userToken}`
      })
    };
    return this.http.post(url, data, this.httpOptions)
  }


  /**
   * Generalized method to get data from database by calling get api based on the url and headers in the httpOptions.
   * @param url- url to be passed to api
   * @returns data returned by the api.
   */
  getData(url) {

 //Getting token from sessionStorage stored after login successful and setting the authorization of httpheaders to this token
 this.userToken = JSON.parse(sessionStorage.getItem('userToken'));
 this.httpOptions = {
   headers: new HttpHeaders({

     'Authorization': `${this.userToken}`
   })
 };

    return this.http.get(url, this.httpOptions);

  }

  /**
   * Generalized method to delete data from database by calling delete api based on the url and headers in the httpOptions.
   * @param url- url to be passed to api
   * @returns data returned by the api.
   */
  deleteData(url) {
    console.log(url);
     //Getting token from sessionStorage stored after login successful and setting the authorization of httpheaders to this token
     this.userToken = JSON.parse(sessionStorage.getItem('userToken'));
     this.httpOptions = {
       headers: new HttpHeaders({
 
         'Authorization': `${this.userToken}`
       })
     };

    return this.http.delete(url, this.httpOptions);
  }

  /**
  * Generalized method to edit data into database by calling patch api based on the url and data and headers in the httpOptions.
  * @param url- url to be passed to api
  * @param data- data parameter to be passed to api
  * @returns data returned by the api.
  */
  patchData(url, data) {
     //Getting token from sessionStorage stored after login successful and setting the authorization of httpheaders to this token
     this.userToken = JSON.parse(sessionStorage.getItem('userToken'));
     this.httpOptions = {
       headers: new HttpHeaders({
 
         'Authorization': `${this.userToken}`
       })
     };
    return this.http.patch(url, data, this.httpOptions);
  }
}
