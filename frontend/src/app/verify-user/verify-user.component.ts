/**
 * Component to verify the user after successful signup. It takes email and token from the user and navigates to the
 * signin page in case the validation is successful
 */
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verify-user',
  templateUrl: './verify-user.component.html',
  styleUrls: ['./verify-user.component.css']
})
export class VerifyUserComponent implements OnInit {
 
email='';
token;


  constructor(private dataService : DataService,private router:Router) {

   }

  ngOnInit() {
  }

  /**
   * Method to verify the user.
   * It creates the post data by using the values as entered by the user and calls the
   * postData method of dataService. In case of successful verification, it navigates to the signin page.
   */
  verify()
  {
    const postData=
    {
     
      email :this.email,
      token:this.token

    }
    this.dataService.postData(`${environment.app_url}/users/verify`,postData)
    .subscribe(data=>
      {
        console.log(data);
       
       console.log(data['response'].code);
        if(1==data['response'].code)
        {
          this.router.navigate(['signin']);
        }

      });
  }

}
