/**Component for Showing libraries shared by the logged in user and also to unshare a particular library with mutiple users */
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DatePipe } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Router } from '@angular/router';
declare const $: any;
@Component({
  selector: 'app-libraries-shared-by-me',
  templateUrl: './libraries-shared-by-me.component.html',
  styleUrls: ['./libraries-shared-by-me.component.scss']
})
export class LibrariesSharedByMeComponent implements OnInit {

  /** 
   * Initializing ng2-smart table settings with the required columns
   */
  settings = {
    width: '30%',
    selectMode: 'multi',
    columns: {
      userName: {
        title: 'UserName',
        filter: true,
        width: '98%'
      }

    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,

    },
    attr: {
      class: 'table table-bordered'
    }
  };
  rows;
  userId = sessionStorage.getItem('userId');
  libraries = [];
  message;
  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router) {
    // calling getLibrariesSharedByMe to get all the libraries shared by the logged in user
    this.getLibrariesSharedByMe();

  }
  checkedRows;

  /**
   * method to set the checkedRows variable 
   * everytime the user selects a row
   * 
   * @param event-the event performed by the user
   * @returns void
   * 
   */
  onUserRowSelect(event) {
    this.checkedRows = event.selected;

  }

  /**
   * method to unshare the shared library from the user
   * It first converts the checkedRows into comma separated checkedRowsId
   * @returns void
   * 
   */
  unShare() {
    var checkedRowsId;
    for (var i = 0; i < this.checkedRows.length; i++) {
      if (i == 0 && i != this.checkedRows.length - 1)
        checkedRowsId = this.checkedRows[i].userId + ',';
      else if (i == 0 && i == this.checkedRows.length - 1) {
        checkedRowsId = this.checkedRows[i].userId;
      }
      else if (i == this.checkedRows.length - 1) {
        checkedRowsId = checkedRowsId + this.checkedRows[i].userId;
      }
      else {
        checkedRowsId = checkedRowsId + this.checkedRows[i].userId + ',';
      }
    }
    let url = `${environment.app_url}/unshare/${this.clickedLibraryId}/users/${checkedRowsId}`;
    this.dataService.deleteData(url).subscribe(data => {
      console.log(data);

      this.message = data['message'];

      this.getLibrariesSharedByMe();
     

    });


  }

  /**
   * Method to refresh the page once the user has unshared the library
   */
  refreshAfterUnshare()
  {
    $(this).parents('.card').addClass('closed').fadeOut();
      
      this.router.navigate(['library/sharedByMe']);
      window.location.reload(true);

  }

  getLibrariesSharedByMe() {

    let url = `${environment.app_url}/users/${this.userId}/sharedlibraries`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data);
      if (data instanceof Array)
        for (var i = 0; i < data.length; i++) {
          var libObject =
            {};

          var usersArray = [];
          var libraryFilter = [];
          libraryFilter = this.libraries.filter(
            library => library.id == data[i].libraryId);
          if (libraryFilter.length == 0) {
            var users = {};
            libObject['id'] = data[i].libraryId;
            libObject['libraryName'] = data[i].libraryName;
            users['userId'] = data[i].sharedUserId;
            users['userName'] = data[i].sharedUserName;
            usersArray.push(users);

            for (var j = i + 1; j < data.length; j++) {


              if (data[i].libraryId == data[j].libraryId) {
                var newuser = {};
                newuser['userId'] = data[j].sharedUserId;
                newuser['userName'] = data[j].sharedUserName;
                usersArray.push(newuser);
              }







            }
            libObject['users'] = usersArray;
            this.libraries.push(libObject);
          }
        

        }
      this.rows = this.libraries;
     

    });

  }
  clickedLibraryName;
  clickedLibraryId;
  getClickedLibraryId(libraryName, libraryId) {
    this.clickedLibraryName = libraryName;
    this.clickedLibraryId = libraryId;

  }

  ngOnInit() {
  }

}
