import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibrariesSharedByMeComponent } from './libraries-shared-by-me.component';

describe('LibrariesSharedByMeComponent', () => {
  let component: LibrariesSharedByMeComponent;
  let fixture: ComponentFixture<LibrariesSharedByMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibrariesSharedByMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibrariesSharedByMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
