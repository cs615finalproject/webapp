/**
 * Component to render forgot password page and get email from the user to send the token
 */

import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  email = '';
  token;
  successMessage = '';
  errorMessage = '';


  constructor(private dataService: DataService, private router: Router) {

  }

  ngOnInit() {
  }

  /**
   * Method to create post data by assigning email and calling the postData method of dataservice
   * to send token via email.
   * In case the api gives error code, errormessage is set
   * otherwise navigates to the verifyForgotPassword page.
   * @returns void
   */
  verify() {
    const postData =
    {
      email: this.email

    }
    this.dataService.postData(`${environment.app_url}/users/forgetPassword`, postData)
      .subscribe(data => {

        if (1 !== data['code']) {
          this.errorMessage = data['message'];
        }
        else {
          this.router.navigate(['verifyForgotPassword', data['message']]);

        }

      });
  }


}
