import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatatableComponent } from '@swimlane/ngx-datatable';
/**
 * Component to show all the references in the trash library and allow the user to empty the trash
 */
import { DatePipe } from '@angular/common';
declare const $: any;
@Component({
  selector: 'app-trash-library-references',
  templateUrl: './trash-library-references.component.html',
  styleUrls: ['./trash-library-references.component.css']
})
export class TrashLibraryReferencesComponent implements OnInit {

  libraries;
  userId = sessionStorage.getItem('userId');
  selectedLibraryForMovingReferences;
  temp = [];
  id: number;
  name;
  rows;
  selected = [];
  rowSelected;
  message;
  entryTypes = [{ id: 0, name: "Article" }, { id: 1, name: "Book" }, { id: 2, name: "InProceedings" }, { id: 3, name: "InCollection" }];
  rowIndices = {};
  private sub: any;
  checkedRows;

  //Initializing the columns of the ng-2 smart table and making it non selectable
  settings = {

    columns: {
      entryType: {
        title: 'Entry Type',
        filter: true
      },
      author: {
        title: 'Author',
        filter: true
      },
      bookTitle: {
        title: 'Book Title',
        filter: true
      },
      editor: {
        title: 'Editor',
        filter: true
      },
      title: {
        title: 'Title',
        filter: true
      },
      journal: {
        title: 'Journal',
        filter: true
      },
      publisher: {
        title: 'Publisher',
        filter: true
      },
      year: {
        title: 'Year',
        filter: true
      },
      volume: {
        title: 'Volume',
        filter: true
      },
      dateCreated: {
        title: 'Date Created',
        type: 'date',
        valuePrepareFunction: (date) => {
          if (date) {
            return new DatePipe('en-GB').transform(date, 'dd/MM/yyyy hh:mm');
          }
          return null;
        },

      }
    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,

    },
    attr: {
      class: 'table table-bordered'
    }
  };

  


  constructor(private route: ActivatedRoute, private dataService: DataService, private http: HttpClient) {
    this.getTrashLibraryreferences();
  }

  /**
   * method to get all the references of the trash library
   * by passing the url as required by the api and calling the getData method of dataservice
   * 
   * @returns void
   */
  getTrashLibraryreferences() {


    let url = `${environment.app_url}/users/${this.userId}/trashLibraryReferences`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data)

      this.rows = data;

    });
  }

   /**
   * method to delete all the references from the trash library
   * by the passing the url as required by the api and calling the deleteData method of dataService.
   * Again calling getTrashLibraryreferences to reflect the changes in the table
   * 
   * @returns void
   */
  emptyTrash() {

    let url = `${environment.app_url}/library/${this.userId}/emptyTrash`;
    this.dataService.deleteData(url).subscribe(data => {
      console.log(data);
      this.message = data['message'];

      this.getTrashLibraryreferences();
    });
  }

  
  ngOnInit() {

    //jquery to remove the card when the user clicks on close icon of the main section card
   
    $('.boxs-close').on('click', function () {
      $(this).parents('.card').addClass('closed').fadeOut();
    });


  }





 



}
