import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrashLibraryReferencesComponent } from './trash-library-references.component';

describe('TrashLibraryReferencesComponent', () => {
  let component: TrashLibraryReferencesComponent;
  let fixture: ComponentFixture<TrashLibraryReferencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrashLibraryReferencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrashLibraryReferencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
