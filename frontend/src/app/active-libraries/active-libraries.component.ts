/**
 * Component for getting all the references of active library and 
 * doing operations on those references
 */
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { DatePipe } from '@angular/common';

declare const $: any;

@Component({
  selector: 'app-active-libraries',
  templateUrl: './active-libraries.component.html',
  styleUrls: ['./active-libraries.component.scss']
})
export class ActiveLibrariesComponent implements OnInit {
  libraries;
  activeLibraryName;
  message;
  userId = sessionStorage.getItem('userId');
  userinfo;
  dropdownSettings;
  dropdownList;
  selectedItems;
  selectedLibraryForMovingReferences;
  temp = [];
  activeLibraryId: number;
  name;
  rows;
  entryType;
  author;
  bookTitle;
  editor;
  title;
  journal;
  publisher;
  year;
  volume;
  rowIndices = {};
  entryMapping = ['Article', 'Book', 'InProceedings', 'InCollection'];
  entryTypes = [{ id: 0, name: 'Article' }, { id: 1, name: 'Book' }, { id: 2, name: 'InProceedings' }, { id: 3, name: 'InCollection' }];
  styleGuide = {
    caretClass: 'caret',
    selectBoxClass: 'dropdown-wrapper',
    selectMenuClass: 'dropdown',
    optionsClass: 'option'
  };
  checkedRows;

  /**
   * ng2-smart table settings
   * allowed multiselecvt option to be true
   */
  settings = {
    selectMode: 'multi',
    columns: {
      entryType: {
        title: 'Entry Type',
        filter: true,
        valuePrepareFunction: (entryType) => {
          if (entryType !== undefined && entryType !== null) {
            if (entryType instanceof Array) {
              return entryType[0].id;

            }

            else {
              return this.entryMapping[entryType];
            }
          }
          else {
            return null;
          }
        },

      },
      author: {
        title: 'Author',
        filter: true
      },
      bookTitle: {
        title: 'Book Title',
        filter: true
      },
      editor: {
        title: 'Editor',
        filter: true
      },
      title: {
        title: 'Title',
        filter: true
      },
      journal: {
        title: 'Journal',
        filter: true
      },
      publisher: {
        title: 'Publisher',
        filter: true
      },
      year: {
        title: 'Year',
        filter: true
      },
      volume: {
        title: 'Volume',
        filter: true
      },
      dateCreated: {
        title: 'Date Created',
        type: 'date',
        valuePrepareFunction: (date) => {
          if (date) {
            return new DatePipe('en-GB').transform(date, 'dd/MM/yyyy hh:mm');
          }
          return null;
        },

      }
    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,

    },
    attr: {
      class: 'table table-bordered'
    }
  };

  /**
   * method to set the checkedRows variable 
   * everytime the user selects a row
   * 
   * @param event-the event performed by the user
   * @returns void
   * 
   */
  onUserRowSelect(event) {
    this.checkedRows = event.selected;
    console.log(this.checkedRows[0]);

  }


  /**
   * Constructor to declare dataservice for calling all the apis 
   * and to get all the active library references.
   * 
   * @param dataService-So that all methods can call the APi through dataservice
   * @returns void
   * 
   */
  constructor(private dataService: DataService) {

    this.getLibraries();
    this.getActiveLibraryReferences();
  }

  /**
   * Method to call the active library references API and
   * and settings rows, message and activeLibraryName
   * variables according to the returned data
   * 
   * @returns void
   * 
   */
  getActiveLibraryReferences() {
    let url = `${environment.app_url}/users/${this.userId}/activeLibraryReferences`;

    this.dataService.getData(url)
      .subscribe(data => {

        console.log(data);
        if (data['response'].code == -1) {
          this.activeLibraryName = "You don't have any active library"
        }
        else {
          this.rows = data['reference'];
          this.activeLibraryName = "Your Active Library--"+data['libraryName'];
          this.activeLibraryId = data['libraryID'];
        }


      }
      );
  }

  /**
   * Method to set the dropdown setting of entryTypes drop down
   * everytime the user clicks on Add reference/Edit reference button
   * 
   * @returns void
   * 
   */
  getEntryTypes() {
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'Id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      closeDropDownOnSelection: true
    };


  }


  /**
  * Method to get all the libraries 
  * and setting the dropdownList to the array returned by the API
  * Also, initializing the dropdownSettings of the libraries dropdwon.
  * 
  * @returns void
  * 
  */
  getLibraries() {

    let url = `${environment.app_url}/users/${this.userId}/library`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data);
      this.dropdownList = data;
      this.dropdownSettings = {
        singleSelection: true,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 3,
        allowSearchFilter: true,
        closeDropDownOnSelection: true
      };
    });
  }


  /**
   * Method to perform opeartions on initialization.
   * It is mainly used for closing the card everytime the user clicks on close button
   * 
   * @returns void
   * 
   */
  ngOnInit() {

    $('.boxs-close').on('click', function () {
      $(this).parents('.card').addClass('closed').fadeOut();
    });


  }


  /**
   * Method to create new references by calling the post API
   * and setting the url as required by the api.
   * 
   * @returns void
   * 
   */
  createReference() {

    let url = `${environment.app_url}/library/${this.activeLibraryId}/references`;

    let postData =
    {
      entryType: +this.entryType,
      author: this.author,
      bookTitle: this.bookTitle,
      editor: this.editor,
      title: this.title,
      journal: this.journal,
      publisher: this.publisher,
      year: String(this.year), //Converting year to string as in html, input type-number was used
      volume: this.volume
    };

    this.dataService.postData(url, postData).subscribe(data => {
      console.log(data)
      // this.rows=data;
      if (1 == data['response'].code) {
        this.message = "New Reference Created";
      }

      this.entryType = null;
      this.author = null;
      this.bookTitle = null;
      this.editor = null;
      this.title = null;
      this.journal = null;
      this.publisher = null;
      this.year = null;
      this.volume = null;
      this.getActiveLibraryReferences();
    });

  }

  /**
  * Method to move references from one library to another library
  * First converting the checkedRows array into comma separated ids
  * and passing the ids to the url.
  * Again calling getActiveLibraryReferences so that the newly 
  * created reference is reflected in the table.
  * 
  * @returns void
  * 
  */
  moveReferencesToAnotherLibrary() {
    let checkedRowsId = this.getCheckedrowsId();
    let url = `${environment.app_url}/library/${this.checkedRows[0].libraryId}/${this.selectedItems[0].id}/referencesMultiple/${checkedRowsId}`;
    this.dataService.patchData(url, {}).subscribe(data => {
      console.log(data)
      this.message=data['message'];
      this.getActiveLibraryReferences();
    });

  }

  /**
 * Method to edit a reference
 * Creating patchData by assigning the selected rows attributes
 * and passing the patchData to the url.
 * Again calling getActiveLibraryReferences so that the edited
 * reference is reflected in the table.
 * 
 * @returns void
 * 
 */
  editReference() {

    let url = `${environment.app_url}/library/${this.checkedRows[0].libraryId}/references/${this.checkedRows[0].id}`;


    let patchData =
    {

      entryType: +this.checkedRows[0].entryType,
      author: this.checkedRows[0].author,
      bookTitle: this.checkedRows[0].bookTitle,
      editor: this.checkedRows[0].editor,
      title: this.checkedRows[0].title,
      journal: this.checkedRows[0].journal,
      publisher: this.checkedRows[0].publisher,
      year: String(this.checkedRows[0].year), //Converting year to string as in html, input type-number was used
      volume: this.checkedRows[0].volume,

    };

    this.dataService.patchData(url, patchData).subscribe(data => {
      console.log(data)
      this.getActiveLibraryReferences();
    });

  }

  /**
 * Method to delete multiple references
 * Appending the selected rows in the url as well as the library id of the active library
 * Again calling getActiveLibraryReferences so that the edited
 * reference is reflected in the table.
 * 
 * @returns void
 * 
 */
  deleteReferences() {

    let checkedRowsId = this.getCheckedrowsId();
    let url = `${environment.app_url}/library/${this.activeLibraryId}/referencesMultiple/${checkedRowsId}`;
    this.dataService.deleteData(url).subscribe(data => {
      console.log(data);


      this.getActiveLibraryReferences();
    });


  }


   /**
 * Generalized method to convert the the selected rows
 * array into comma separated selectedrows(references) id.
 * 
 * @returns void
 * 
 */
  getCheckedrowsId() {
    var checkedRowsId;
    for (var i = 0; i < this.checkedRows.length; i++) {
      if (i == 0 && i != this.checkedRows.length - 1)
        checkedRowsId = this.checkedRows[i].id + ',';
      else if (i == 0 && i == this.checkedRows.length - 1) {
        checkedRowsId = this.checkedRows[i].id;
      }
      else if (i == this.checkedRows.length - 1) {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id;
      }
      else {
        checkedRowsId = checkedRowsId + this.checkedRows[i].id + ',';
      }
    }
    return checkedRowsId;
  }









}
