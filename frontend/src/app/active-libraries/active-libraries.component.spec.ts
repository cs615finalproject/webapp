import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveLibrariesComponent } from './active-libraries.component';

describe('ActiveLibrariesComponent', () => {
  let component: ActiveLibrariesComponent;
  let fixture: ComponentFixture<ActiveLibrariesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveLibrariesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveLibrariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
