import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GetAllLibrariesComponent } from './get-all-libraries/get-all-libraries.component';


import { ListReferencesComponent } from './references/list-references.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { VerifyUserComponent } from './verify-user/verify-user.component';
import { VerifyForgotPasswordComponent } from './verify-forgot-password/verify-forgot-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

import { ActiveLibrariesComponent } from './active-libraries/active-libraries.component';
import { TrashLibraryReferencesComponent } from './trash-library-references/trash-library-references.component';
import { UnFiledLibraryReferencesComponent } from './un-filed-library-references/un-filed-library-references.component';
import { LibrariesSharedByMeComponent } from './libraries-shared-by-me/libraries-shared-by-me.component';
import { LibrariesSharedWithMeComponent } from './libraries-shared-with-me/libraries-shared-with-me.component';
import { GetAllReferencesComponent } from './get-all-references/get-all-references.component';
import { MyAccountDetailsComponent } from './my-account-details/my-account-details.component';
import { AuthenticationGuard } from './authentication.guard';

//Defining the paths and the corresponding components.
//Used authentication guard so that only the logged in user can see the libraries and the references
const routes: Routes = [
    {
        path: 'library/showLibraries',
        component: GetAllLibrariesComponent,
        canActivate: [AuthenticationGuard]

    },
    {
        path: '',
        component: SigninComponent,
       

    },
    {
        path: 'library/myAccount',
        component: MyAccountDetailsComponent,
        canActivate: [AuthenticationGuard]

    },
    {
        path: 'library/activeLibrary',
        component: ActiveLibrariesComponent,
        canActivate: [AuthenticationGuard]

    },
    {
        path: 'library/trashLibrary',
        component: TrashLibraryReferencesComponent,
        canActivate: [AuthenticationGuard]

    },
    {
        path: 'library/sharedByMe',
        component: LibrariesSharedByMeComponent,
        canActivate: [AuthenticationGuard]

    },
    {
        path: 'library/sharedWithMe',
        component: LibrariesSharedWithMeComponent,
        canActivate: [AuthenticationGuard]

    },
    {
        path: 'library/unFiledLibrary',
        component: UnFiledLibraryReferencesComponent,
        canActivate: [AuthenticationGuard]

    },
    {
        path: 'library/seeAllreferences',
        component: GetAllReferencesComponent,
        canActivate: [AuthenticationGuard]

    },
  
    {
        path: 'signin',
        component: SigninComponent,

    },
    {
        path: 'verifyForgotPassword/:message',
        component: VerifyForgotPasswordComponent,

    },
    {
        path: 'forgotPassword',
        component: ForgotPasswordComponent,

    },
  
    {
        path: 'verify',
        component: VerifyUserComponent,

    },
    {
        path: 'signup',
        component: SignupComponent,

    },
    {
        path: 'references/:libraryId/:libraryName', 
        component: ListReferencesComponent,
        canActivate: [AuthenticationGuard]
    }


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
