/**
 * Component for showing the sign up page to the user and navigating the user to the
 * verify user page in case the signup is successful, otherwise, showing the error message
 */
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
userName;
email='';
password;
confirmPassword;
errorMessage;
  constructor(private dataService : DataService,private router:Router) {

   }

  ngOnInit() {
  }

  /**
   * Method to create post data using the values entered by the user and calling post data method
   * of dataservice. It navigates to the verifyUser component in case the signup is successful 
   * otherwise displays the error message.
   * 
   * @returns void
   */
  signup()
  {
    // Checking if the user entered the same value for password and confirm password
    if(this.password == this.confirmPassword)
    {
    const postData=
    {
      name:this.userName,
      email :this.email,
      password:this.password

    }
    this.dataService.postData(`${environment.app_url}/users/register`,postData)
    .subscribe(data=>
      {
        console.log(data);
       
       console.log(data['code']);
        if(1==data['code'])
        {
          this.router.navigate(['verify']);
        }
        else
        {
          this.errorMessage=data['message'];
        }

      });
    }
    else{
      this.errorMessage= "Password and confirm Password are not same"

    }
  }


}
