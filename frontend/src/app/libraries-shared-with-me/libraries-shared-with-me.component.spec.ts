import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LibrariesSharedWithMeComponent } from './libraries-shared-with-me.component';

describe('LibrariesSharedWithMeComponent', () => {
  let component: LibrariesSharedWithMeComponent;
  let fixture: ComponentFixture<LibrariesSharedWithMeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LibrariesSharedWithMeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LibrariesSharedWithMeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
