/**Component to display all the libraries that are shared with 
 * the logged in user and also the user will be able to rename that libfrary and view references of that library */
import { Component, OnInit } from '@angular/core';

import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare const $: any;
@Component({
  selector: 'app-libraries-shared-with-me',
  templateUrl: './libraries-shared-with-me.component.html',
  styleUrls: ['./libraries-shared-with-me.component.css']
})
export class LibrariesSharedWithMeComponent implements OnInit {

  libraryName;
  userId = sessionStorage.getItem('userId');
  askUserBeforeUnsharing() {
    $('#deleteModal').show();
  }
  selectedLibraryForDeletion = { "key": "unfiled", "value": 2 };
  styleGuide = {
    caretClass: 'caret',
    selectBoxClass: 'dropdown-wrapper',
    selectMenuClass: 'dropdown',
    optionsClass: 'option'
  };
  dropdownList;
  selectedItems;
  dropdownSettings = {};
  settings = {

    columns: {
      libraryName: {
        title: 'Library Name',
        filter: true
      },
      sharedUserName: {
        title: 'Shared By',
        filter: true
      }
    },
    mode: 'external',
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          name: 'viewReferences',
          title: 'References ',
        }

      ],
    },
    attr: {
      class: 'table table-bordered'
    }
  };
  //userLibraries;
  rowSelected;
  newLibraryName;
  message;
  trashOrUnfiledLibraryForReferences = [{ "key": "trash", "value": 1 },
  { "key": "unfiled", "value": 2 }];
  isLoggedIn = false;

  rows;
  selected = [];
  showRenameModal = false;
  checkedRows;

  /**
   * Method to be called when the user clicks on view references link on the table
   * 
   * @param event-user click event 
   */
  onCustom(event) {

    var str1 = "viewReferences";

    var str2 = `${event.action}`;
    var index = str1.localeCompare(str2);


    if (index == 0) {
      this.router.navigate(['/references', `${event.data.id}`, `${event.data.name}`]);

    }
  }
  checkedRowsId;

  /**
   * method to set the checkedRows variable 
   * everytime the user selects a row
   * 
   * @param event-the event performed by the user
   * @returns void
   * 
   */
  onUserRowSelect(event) {
    this.checkedRows = event.selected;
  }



  /**
   * method to change the active library and displaying the message
   * Also, the getLibraries method is called again to reflect the changes
   * @param event-the event performed by the user
   * @returns void
   * 
   */
  changeActiveLibrary() {

    let url = `${environment.app_url}/users/${this.userId}/makeActive/${this.checkedRows[0].id}`;
    this.dataService.patchData(url, {}).subscribe(data => {
      console.log(data)

      if (1 == data['count']) {
        this.message = this.checkedRows[0].name + " is now the active library";
      }
      this.getLibraries();
    });

  }
  constructor(private dataService: DataService, private router: Router, private formBuilder: FormBuilder) {
    this.isLoggedIn = dataService.isLoggedIn();
    //getLibraries method is called to load the table with all the libraries shared with logged in user

    this.getLibraries();

  }

  ngOnInit() {

 $('.boxs-close').on('click', function () {
      $(this).parents('.card').addClass('closed').fadeOut();
    });
  }

  /**
   * method to get all the libraries shared with logged in user
   * @returns void
   * 
   */
  getLibraries() {
    let url = `${environment.app_url}/users/${this.userId}/sharedlibrariesWithUser`;
    this.dataService.getData(url).subscribe(data => {
      console.log(data)
      this.rows = data;
      console.log(data);
    });



  }

  

   /**
   * method to rename a particular library according the row selected by the user
   * @returns void
   * 
   */
 
  renameLibrary() {
    const patchData =
    {
      name: this.newLibraryName,


    }


    this.dataService.patchData(`${environment.app_url}/libraries/${this.checkedRows[0].id}/rename`, patchData)
      .subscribe(data => {

        console.log(data);
        this.message = data['response'].message;

        this.getLibraries();

      }
      );
  }
 



}



