/**
 * Component to verify forgot password based on the email, token and password received from the user.
 */

import { Component, OnInit } from '@angular/core';
import { DataService } from '../data-service/dataService';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-verify-forgot-password',
  templateUrl: './verify-forgot-password.component.html',
  styleUrls: ['./verify-forgot-password.component.css']
})
export class VerifyForgotPasswordComponent implements OnInit {

  token;
  email = '';
  password;
  messageFromServer;
  sub: any;
  message;
  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router) {
    //Sets the success message as passed by the forgot-password component
    this.sub = this.route.params.subscribe(params => {
      this.message = params['message']; // (+) converts string 'id' to a number
      console.log(this.message);



    });
  }

  ngOnInit() {
  }

  /**
   * Method to verify the forgot password.
   * It constructs the postdata using the values entered by the user
   * and calls the postData method of dataService.
   * If verification is successful, it asks the user to login, otherwise it displays the error message.
   * 
   * @returns void
   */
  verify() {
    this.message = "";
    const postData =
    {

      email: this.email,
      token: this.token, //user receives the token in mail and enters the value here
      password: this.password

    }
    this.dataService.postData(`${environment.app_url}/users/verifyForgetPassword`, postData)
      .subscribe(data => {
        console.log(data);
       

        if (1 == data['response'].code) {
         
          this.message = "Verification is successfull. Please login"
        }
        else {

          this.messageFromServer = data['response'].message;
        }


      });
  }

}
